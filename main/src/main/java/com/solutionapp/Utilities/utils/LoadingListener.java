package com.solutionapp.Utilities.utils;

public interface LoadingListener {

	public void onLoadingStarted();

	public void onLoadingFinished();

	public void onProgressUpdated(int percentLoaded);
}
