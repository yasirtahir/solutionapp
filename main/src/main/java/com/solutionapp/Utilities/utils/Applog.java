package com.solutionapp.Utilities.utils;

import android.util.Log;

public class Applog {

	public static void Debug(String msg) {
		if (isEmptyOrNull(msg))
			return;
		if (Common.DEBUG)
			Log.d("Application", msg);
	}

	public static void Error(String msg) {
		if (isEmptyOrNull(msg))
			return;
		if (Common.DEBUG)
			Log.e("Application", msg);
	}

	private static boolean isEmptyOrNull(String string) {
		if (string == null)
			return true;
		return (string.trim().length() <= 0);
	}
}
