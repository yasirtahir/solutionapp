package com.solutionapp.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;
import com.solutionapp.retro.entities.CategoryEntity;
import com.solutionapp.retro.entities.CategoryWrapper;
import com.solutionapp.retro.entities.FAQDataWrapper;
import com.solutionapp.retro.entities.FAQEntity;
import com.solutionapp.retro.entities.LocationEntity;
import com.solutionapp.retro.entities.LocationWrapper;
import com.solutionapp.retro.entities.SelectedService;
import com.solutionapp.retro.entities.SelectedServiceWrapper;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.retro.entities.ServiceWrapper;

import java.util.ArrayList;

/**
 * Class that can be extended to make available simple preference
 * setter/getters.
 * <p/>
 * Should be extended to provide Facades.
 */
public class PreferenceHelper {


    private Context context;
    private static final String FILENAME = "preferences";
    public static final String KEY_SERVICES = "SolutionServices";
    public static final String KEY_CATEGORIES = "SolutionCategories";
    public static final String KEY_LOCATIONS = "SolutionLocations";
    public static final String KEY_FAQs = "SolutionFAQs";
    public static final String KEY_SELECTED_LOCATION = "SolutionSelectedLocation";
    public static final String KEY_SCHEDULED_SERVICES = "SolutionScheduledService";

    public PreferenceHelper(Context context) {
        this.context = context;
    }

    // ////////////////////////////////////////////////////////////////////////////////////

    /**
     * APP SPECIFIC PREFERENCES
     */

    // ////////////////////////////////////////////////////////////////////////////////////

    public LocationEntity getSelectedServices() {
        return new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_SELECTED_LOCATION), LocationEntity.class);
    }

    public void putSelectedServices(LocationEntity serviceEntity) {
        putStringPreference(context, FILENAME, KEY_SELECTED_LOCATION, new GsonBuilder().create().toJson(serviceEntity));
    }

    public ArrayList<ServiceEntity> getServices() {
        ServiceWrapper wrapper = new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_SERVICES), ServiceWrapper.class);
        if (wrapper != null && wrapper.getServiceEntities() != null)
            return wrapper.getServiceEntities();
        else
            return null;
    }

    public void putServices(ArrayList<ServiceEntity> serviceEntityArrayList) {
        ServiceWrapper wrapper = new ServiceWrapper();
        wrapper.setServiceEntities(serviceEntityArrayList);
        putStringPreference(context, FILENAME, KEY_SERVICES, new GsonBuilder().create().toJson(wrapper));
    }

    public ArrayList<SelectedService> getScheduledServices() {
        SelectedServiceWrapper wrapper = new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_SCHEDULED_SERVICES), SelectedServiceWrapper.class);
        if (wrapper != null && wrapper.getSelectedServices() != null)
            return wrapper.getSelectedServices();
        else
            return null;
    }

    public void putScheduledServices(ArrayList<SelectedService> serviceEntityArrayList) {
        SelectedServiceWrapper wrapper = new SelectedServiceWrapper();
        wrapper.setSelectedServices(serviceEntityArrayList);
        putStringPreference(context, FILENAME, KEY_SCHEDULED_SERVICES, new GsonBuilder().create().toJson(wrapper));
    }

    public ArrayList<CategoryEntity> getCategories() {
        CategoryWrapper wrapper = new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_CATEGORIES), CategoryWrapper.class);
        if (wrapper != null && wrapper.getCategoryEntities() != null)
            return wrapper.getCategoryEntities();
        else
            return null;
    }

    public void putCategories(ArrayList<CategoryEntity> categoryEntityArrayList) {
        CategoryWrapper wrapper = new CategoryWrapper();
        wrapper.setCategoryEntities(categoryEntityArrayList);
        putStringPreference(context, FILENAME, KEY_CATEGORIES, new GsonBuilder().create().toJson(wrapper));
    }

    public ArrayList<LocationEntity> getLocations() {
        LocationWrapper wrapper = new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_LOCATIONS), LocationWrapper.class);
        if (wrapper != null && wrapper.getLocationEntityArrayList() != null)
            return wrapper.getLocationEntityArrayList();
        else
            return new ArrayList<>();
    }

    public void putLocations(ArrayList<LocationEntity> categoryEntityArrayList) {
        LocationWrapper wrapper = new LocationWrapper();
        wrapper.setLocationEntityArrayList(categoryEntityArrayList);
        putStringPreference(context, FILENAME, KEY_LOCATIONS, new GsonBuilder().create().toJson(wrapper));
    }

    public ArrayList<FAQEntity> getFAQs() {
        FAQDataWrapper wrapper = new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_FAQs), FAQDataWrapper.class);
        if (wrapper != null && wrapper.getFaq_content() != null)
            return wrapper.getFaq_content();
        else
            return null;
    }

    public void putFAQs(ArrayList<FAQEntity> categoryEntityArrayList) {
        FAQDataWrapper wrapper = new FAQDataWrapper();
        wrapper.setFaq_content(categoryEntityArrayList);
        putStringPreference(context, FILENAME, KEY_FAQs, new GsonBuilder().create().toJson(wrapper));
    }

    // ////////////////////////////////////////////////////////////////////////////////////

    /**
     * GENERAL PREFERENCES
     */

    // ////////////////////////////////////////////////////////////////////////////////////

    protected void putStringPreference(Context context, String prefsName, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    protected String getStringPreference(Context context, String prefsName, String key) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    protected void putBooleanPreference(Context context, String prefsName, String key, boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    protected boolean getBooleanPreference(Context context, String prefsName, String key) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    protected void putIntegerPreference(Context context, String prefsName, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences( prefsName, Activity.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    protected int getIntegerPreference(Context context, String prefsName, String key) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        return preferences.getInt(key, -1);
    }

    protected void putLongPreference(Context context, String prefsName, String key, long value) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    protected long getLongPreference(Context context, String prefsName,String key) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        return preferences.getLong(key, Integer.MIN_VALUE);
    }

    protected void putFloatPreference(Context context, String prefsName,String key, float value) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    protected float getFloatPreference(Context context, String prefsName, String key) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        return preferences.getFloat(key, Float.MIN_VALUE);
    }

    protected void removePreference(Context context, String prefsName,String key) {
        SharedPreferences preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
    }
}
