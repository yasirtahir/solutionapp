package com.solutionapp.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andreabaccega.formedittextvalidator.Validator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.solutionapp.R;
import com.solutionapp.Utilities.GPSLocationTracker;
import com.solutionapp.Utilities.PreferenceHelper;
import com.solutionapp.Utilities.UIHelper;
import com.solutionapp.fragments.HomeFragment;
import com.solutionapp.fragments.MyAccountParentFragment;
import com.solutionapp.fragments.MyLocationFragment;
import com.solutionapp.fragments.MyScheduleFragment;
import com.solutionapp.retro.entities.CategoryData;
import com.solutionapp.retro.entities.CategoryEntity;
import com.solutionapp.retro.entities.FAQEntity;
import com.solutionapp.retro.entities.FAQResponse;
import com.solutionapp.retro.entities.LocationEntity;
import com.solutionapp.retro.entities.ServiceData;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.retro.model.WebServiceFactory;
import com.solutionapp.ui.views.AnyEditTextView;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import roboguice.inject.InjectView;

public class MainActivity extends DockActivity implements OnClickListener {

    @InjectView(R.id.header_main)
    public TitleBar titleBar;

    @InjectView(R.id.mainFrameLayout)
    FrameLayout mainFrameLayout;

    @InjectView(R.id.rlPopParentView)
    RelativeLayout rlPopParentView;

    @InjectView(R.id.rlLeftPopParentView)
    RelativeLayout rlLeftPopParentView;

    @InjectView(R.id.btnMyAccount)
    AnyTextView btnMyAccount;

    @InjectView(R.id.btnScheduledService)
    AnyTextView btnScheduledService;

    @InjectView(R.id.btnMyLocation)
    AnyTextView btnMyLocation;

    @InjectView(R.id.btnContact)
    AnyTextView btnContact;

    @InjectView(R.id.btnUberPickUp)
    AnyTextView btnUberPickUp;

    @InjectView(R.id.btnTapToCall)
    AnyTextView btnTapToCall;

    @InjectView(R.id.btnDirections)
    AnyTextView btnDirections;

    @InjectView(R.id.btnLeftUberPickUp)
    AnyTextView btnLeftUberPickUp;

    @InjectView(R.id.rlContactUs)
    RelativeLayout rlContactUs;

    @InjectView(R.id.btnCallSolution)
    AnyTextView btnCallSolution;

    @InjectView(R.id.btnSendInformation)
    AnyTextView btnSendInformation;

    @InjectView(R.id.edtTxtName)
    AnyEditTextView edtTxtName;

    @InjectView(R.id.edtTxtEmail)
    AnyEditTextView edtTxtEmail;

    @InjectView(R.id.edtTxtMessage)
    AnyEditTextView edtTxtMessage;

    @InjectView(R.id.imgBlurBackground)
    ImageView imgBlurBackground;

    private PreferenceHelper preferenceHelper;
    private static final int ZOOM_LEVEL = 11;
    private GoogleMap mMapFragment;
    private GPSLocationTracker locationTracker;
    public static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dock);
        preferenceHelper = new PreferenceHelper(MainActivity.this);
        setListeners();
        if (savedInstanceState == null)
            initFragment();
        syncDataInBackground();
    }

    public void initFragment() {
        addFragment(HomeFragment.newInstance(), HomeFragment.class.getSimpleName());
    }

    private void setListeners() {
        // Right Menu Items
        btnMyAccount.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        btnMyLocation.setOnClickListener(this);
        btnScheduledService.setOnClickListener(this);
        btnUberPickUp.setOnClickListener(this);

        // Left Menu Items
        btnDirections.setOnClickListener(this);
        btnLeftUberPickUp.setOnClickListener(this);

        // Contact Us Buttons
        btnCallSolution.setOnClickListener(this);
        btnSendInformation.setOnClickListener(this);
        // Empty field validator
        addEmptyStringValidator(edtTxtName, edtTxtEmail, edtTxtMessage);
        // To Close All Menus
        imgBlurBackground.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                rlContactUs.setVisibility(View.GONE);
                rlLeftPopParentView.setVisibility(View.GONE);
                rlPopParentView.setVisibility(View.GONE);
                titleBar.hideMenuDropDown();
                titleBar.hideLeftMenuDropDown();
                imgBlurBackground.setVisibility(View.GONE);
                return true;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        checkRightMenu();
        checkLeftMenu();
        if (locationTracker != null)
            locationTracker.stopUsingGPS();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (locationTracker == null)
            locationTracker = new GPSLocationTracker(MainActivity.this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initMap();
                setUI();
            }
        }, 300);
    }

    private void initMap() {
        try {
            if (mMapFragment == null) {
                mMapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFragmentFrame)).getMap();
                mMapFragment.getUiSettings().setZoomControlsEnabled(false);
                if (mMapFragment == null) {
                    UIHelper.showLongToastInCenter(MainActivity.this, "Sorry! unable to create maps");
                }
            }
        } catch (Exception e) {
            // Don't need to print this catch
        }
    }

    private void placeMarker(String latitude, String longitude, String title) {
        try {
            if (mMapFragment != null) {
                // Clearing previously added things if any
                mMapFragment.clear();
                // User Current Location Pointer
                final LatLng latLong = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                MarkerOptions markerOptions = new MarkerOptions().position(latLong).title(title);
                mMapFragment.addMarker(markerOptions);

                // General Map Feature Settings
                mMapFragment.animateCamera(CameraUpdateFactory.newLatLngZoom(latLong, ZOOM_LEVEL));
                mMapFragment.setMyLocationEnabled(false);
                mMapFragment.getUiSettings().setMyLocationButtonEnabled(false);
                mMapFragment.getUiSettings().setZoomControlsEnabled(false);
            }
        } catch (Throwable t) {
            // Don't need to print this catch
        }
    }

    @Override
    public void onLoadingStarted() {
        if (mainFrameLayout != null) {
            mainFrameLayout.setVisibility(View.VISIBLE);
            titleBar.showProgressBar();
        }
    }

    @Override
    public void onLoadingFinished() {
        mainFrameLayout.setVisibility(View.VISIBLE);
        titleBar.hideProgressBar();
    }

    @Override
    public void onProgressUpdated(int percentLoaded) {

    }

    @Override
    public int getDockFrameLayoutId() {
        return R.id.mainFrameLayout;
    }

    @Override
    public void onBackPressed() {
        if (titleBar.isProgressBarShowing()) {
            UIHelper.showShortToastInCenter(getApplicationContext(), R.string.message_wait);
        } else if (checkRightMenu() && checkLeftMenu())
            super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnMyAccount:
                toggleRightMenu();
                popBackStackTillEntry(1);
                addFragmentWithAnimation(MyAccountParentFragment.newInstance(), MyAccountParentFragment.class.getSimpleName());
                break;
            case R.id.btnMyLocation:
                toggleRightMenu();
                popBackStackTillEntry(1);
                addFragmentWithAnimation(MyLocationFragment.newInstance(), MyLocationFragment.class.getSimpleName());
                break;
            case R.id.btnScheduledService:
                toggleRightMenu();
                popBackStackTillEntry(1);
                addFragmentWithAnimation(MyScheduleFragment.newInstance(), MyScheduleFragment.class.getSimpleName());
                break;
            case R.id.btnContact:
                toggleRightMenu();
                toggleContactUs();
                break;
            case R.id.btnUberPickUp:
                toggleRightMenu();
                LatLng pickUpPosition = (LatLng) btnDirections.getTag();
                openUberApp(String.valueOf(pickUpPosition.latitude) + "," + String.valueOf(pickUpPosition.longitude));
                break;
            case R.id.btnTapToCall:
                toggleLeftMenu();
                UIHelper.showShortToastInCenter(MainActivity.this, "No calling number is available for this right now. Please try again later");
                break;
            case R.id.btnDirections:
                toggleLeftMenu();
                LatLng position = (LatLng) btnDirections.getTag();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + String.valueOf(position.latitude) + "," + String.valueOf(position.longitude)));
                startActivity(intent);
                break;
            case R.id.btnLeftUberPickUp:
                toggleLeftMenu();
                LatLng leftPickUpPosition = (LatLng) btnDirections.getTag();
                openUberApp(String.valueOf(leftPickUpPosition.latitude) + "," + String.valueOf(leftPickUpPosition.longitude));
                break;
            case R.id.btnCallSolution:
                toggleLeftMenu();
                callSolution();
                break;
            case R.id.btnSendInformation:
                if (checkContactValidity()) {
                    sendEmailToSolution();
                }
                break;
            case R.id.imgBlurBackground:
                rlContactUs.setVisibility(View.GONE);
                rlLeftPopParentView.setVisibility(View.GONE);
                rlPopParentView.setVisibility(View.GONE);
                titleBar.hideMenuDropDown();
                titleBar.hideLeftMenuDropDown();
                imgBlurBackground.setVisibility(View.GONE);
                break;
        }
    }

    private boolean checkContactValidity() {
        return edtTxtName.testValidity() && edtTxtEmail.testValidity() && edtTxtMessage.testValidity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_RECOVER_PLAY_SERVICES:
                if (resultCode != Activity.RESULT_OK) {
                    Toast.makeText(this, "Google Play Services must be installed.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void syncDataInBackground() {
        // Get Categories
        WebServiceFactory.getInstance().getCategories(new Callback<Response>() {
            @Override
            public void success(Response string, Response response) {
                try {
                    JSONObject jsonObj = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    JSONObject object1 = jsonObj.getJSONObject("1");
                    JSONObject object2 = jsonObj.getJSONObject("3");
                    JSONObject object3 = jsonObj.getJSONObject("5");
                    JSONObject object4 = jsonObj.getJSONObject("7");

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    jsonObjectArrayList.add(object1);
                    jsonObjectArrayList.add(object2);
                    jsonObjectArrayList.add(object3);
                    jsonObjectArrayList.add(object4);

                    ArrayList<CategoryEntity> arrayList = new ArrayList<>();
                    for (int i = 0; i < jsonObjectArrayList.size(); i++) {
                        JSONObject jsonObject = jsonObjectArrayList.get(i);
                        CategoryEntity entity = new CategoryEntity();
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        entity.setName(name);
                        entity.setDescription(description);

                        JSONObject data = jsonObject.getJSONObject("Data");
                        CategoryData categoryData = new CategoryData();

                        String categoryImage = "";
                        String iconImage = "";
                        String featureImage = "";
                        String hexColor = "";
                        String service_categories_faq = "";
                        JSONArray servicesJsonArray = null;
                        try {
                            categoryImage = data.getJSONArray("category_image").get(0).toString();
                            iconImage = data.getJSONArray("icon_image").get(0).toString();
                            featureImage = data.getJSONArray("feature_image").get(0).toString();
                            hexColor = data.getJSONObject("terms").getString("service_categories_hex");
                            service_categories_faq = data.getJSONObject("terms").getString("service_categories_faq");
                            servicesJsonArray = data.getJSONArray("mb_services");
                        } catch (Throwable t) {
                            // Placed this try catch so that the super try catch won't get break
                        }
                        categoryData.setCategoryImage(categoryImage);
                        categoryData.setIconImage(iconImage);
                        categoryData.setFeatureImage(featureImage);
                        categoryData.setHexColor(hexColor);
                        categoryData.setService_categories_faq(service_categories_faq);

                        if (servicesJsonArray != null) {
                            ArrayList<ServiceEntity> serviceArrayList = new ArrayList<>();
                            for (int j = 0; j < servicesJsonArray.length(); j++) {
                                JSONObject serviceJsonObject = servicesJsonArray.getJSONObject(j);
                                ServiceEntity serviceEntity = new ServiceEntity();
                                String postContent = serviceJsonObject.getString("post_content");
                                String post_title = serviceJsonObject.getString("post_title");
                                serviceEntity.setPostContent(postContent);
                                JSONObject serviceJsonData = serviceJsonObject.getJSONObject("Data");
                                ServiceData serviceData = new ServiceData();
                                String mb_service_price = "";
                                String featured_image = "";
                                try {
                                    mb_service_price = serviceJsonData.getJSONArray("mb_service_price").get(0).toString();
                                    featured_image = serviceJsonData.getString("featured_image");
                                } catch (Throwable t) {
                                    // Placed this try catch so that the super try catch won't get break
                                }
                                serviceData.setMbServicePrice(mb_service_price);
                                serviceData.setFeatured_image(featured_image);
                                serviceEntity.setData(serviceData);
                                serviceEntity.setPostTitle(post_title);
                                serviceArrayList.add(serviceEntity);
                            }
                            categoryData.setMb_services(serviceArrayList);
                        }
                        entity.setCategoryData(categoryData);
                        arrayList.add(entity);
                    }

                    if (arrayList.size() > 0) {
                        preferenceHelper.putCategories(arrayList);
                    }
                } catch (Throwable e) {
                    // Don't need to print this catch
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });

        // Get FAQs
        WebServiceFactory.getInstance().getFAQs(new Callback<FAQResponse>() {
            @Override
            public void success(FAQResponse faqResponse, Response response) {
                if (faqResponse != null) {
                    ArrayList<FAQEntity> faqEntities = new ArrayList<>();

                    for (int i = 0; i < faqResponse.getFaqWrapper0().getData().getFaq_content().size(); i++) {
                        faqResponse.getFaqWrapper0().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper0().getTerm_id());
                    }
                    for (int i = 0; i < faqResponse.getFaqWrapper1().getData().getFaq_content().size(); i++) {
                        faqResponse.getFaqWrapper1().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper1().getTerm_id());
                    }
                    for (int i = 0; i < faqResponse.getFaqWrapper2().getData().getFaq_content().size(); i++) {
                        faqResponse.getFaqWrapper2().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper2().getTerm_id());
                    }
                    for (int i = 0; i < faqResponse.getFaqWrapper3().getData().getFaq_content().size(); i++) {
                        faqResponse.getFaqWrapper3().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper3().getTerm_id());
                    }
                    for (int i = 0; i < faqResponse.getFaqWrapper5().getData().getFaq_content().size(); i++) {
                        faqResponse.getFaqWrapper5().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper5().getTerm_id());
                    }
                    faqEntities.addAll(faqResponse.getFaqWrapper0().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper1().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper2().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper3().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper5().getData().getFaq_content());
                    preferenceHelper.putFAQs(faqEntities);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });

        // Get Locations
        WebServiceFactory.getInstance().getLocations(new Callback<ArrayList<LocationEntity>>() {
            @Override
            public void success(ArrayList<LocationEntity> locationEntities, Response response) {
                if (locationEntities != null && locationEntities.size() > 0) {
                    preferenceHelper.putLocations(locationEntities);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });
    }

    public void toggleRightMenu() {
        rlContactUs.setVisibility(View.GONE);
        if (rlPopParentView.getVisibility() == View.VISIBLE) {
            rlPopParentView.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
            titleBar.hideMenuDropDown();
        } else {
            rlPopParentView.setVisibility(View.VISIBLE);
            imgBlurBackground.setVisibility(View.VISIBLE);
            titleBar.showMenuDropDown(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    rlPopParentView.setVisibility(View.GONE);
                    imgBlurBackground.setVisibility(View.GONE);
                    titleBar.hideMenuDropDown();
                }
            });
        }
    }

    public boolean checkRightMenu() {
        if (rlPopParentView.getVisibility() == View.VISIBLE) {
            titleBar.hideMenuDropDown();
            rlPopParentView.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
            return false;
        }
        if (rlContactUs.getVisibility() == View.VISIBLE) {
            rlContactUs.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
            return false;
        }
        return true;
    }

    public void toggleLeftMenu() {
        if (rlLeftPopParentView.getVisibility() == View.VISIBLE) {
            rlLeftPopParentView.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
            titleBar.hideLeftMenuDropDown();
        } else {
            rlLeftPopParentView.setVisibility(View.VISIBLE);
            imgBlurBackground.setVisibility(View.VISIBLE);
            titleBar.showLeftMenuDropDown(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    rlLeftPopParentView.setVisibility(View.GONE);
                    imgBlurBackground.setVisibility(View.GONE);
                    titleBar.hideLeftMenuDropDown();
                }
            });
        }
    }

    public boolean checkLeftMenu() {
        if (rlLeftPopParentView.getVisibility() == View.VISIBLE) {
            titleBar.hideLeftMenuDropDown();
            rlLeftPopParentView.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
            return false;
        }
        if (rlContactUs.getVisibility() == View.VISIBLE) {
            rlContactUs.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
            return false;
        }
        return true;
    }

    public void setUI() {
        String text;
        if (preferenceHelper.getSelectedServices() != null) {
            LocationEntity entity = preferenceHelper.getSelectedServices();
            text = entity.getTitle().getRendered() + " - Tap to Call";
            btnTapToCall.setText(text);
            btnTapToCall.setOnClickListener(this);
            placeMarker(entity.getLocLat().get(0), entity.getLocLong().get(0), entity.getTitle().getRendered());
            LatLng latLong = new LatLng(Double.parseDouble(entity.getLocLat().get(0)), Double.parseDouble(entity.getLocLong().get(0)));
            btnDirections.setTag(latLong);
            btnLeftUberPickUp.setTag(latLong);
            btnUberPickUp.setTag(latLong);
        } else {
            text = "Current Location";
            btnTapToCall.setText(text);
            btnTapToCall.setOnClickListener(null);
            LatLng latLong = new LatLng(locationTracker.getLatitude(), locationTracker.getLongitude());
            placeMarker(String.valueOf(latLong.latitude), String.valueOf(latLong.longitude), "Your Current Location");
            btnDirections.setTag(latLong);
            btnLeftUberPickUp.setTag(latLong);
            btnUberPickUp.setTag(latLong);
        }
    }

    public void openUberApp(String latLng) {
        try {
            PackageManager pm = MainActivity.this.getPackageManager();
            pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
            String uri = "uber://?action=setPickup&pickup=" + latLng;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            // Uber App isn't installed on the device
            try {
                // If Play Store is installed on the device
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ubercab")));
            } catch (ActivityNotFoundException a) {
                // If Play Store isn't installed or isn't updated
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.ubercab")));
            }
        }
    }

    private void toggleContactUs() {
        if (rlContactUs.getVisibility() == View.VISIBLE) {
            rlContactUs.setVisibility(View.GONE);
            imgBlurBackground.setVisibility(View.GONE);
        } else {
            rlContactUs.setVisibility(View.VISIBLE);
            imgBlurBackground.setVisibility(View.VISIBLE);
        }
    }

    private void addEmptyStringValidator(AnyEditTextView... allFields) {

        for (AnyEditTextView field : allFields) {
            field.addValidator(new EmptyStringValidator());
        }
    }

    private class EmptyStringValidator extends Validator {

        public EmptyStringValidator() {
            super("The field must not be empty");
        }

        @Override
        public boolean isValid(EditText et) {
            return et.getText().toString().trim().length() >= 1;
        }
    }

    private void sendEmailToSolution() {
        String body = "Name: " + edtTxtName.getText().toString().trim() + "\n" +
                "Email Address: " + edtTxtEmail.getText().toString().trim() + "\n" +
                "Message: " + edtTxtMessage.getText().toString().trim();
        Intent send = new Intent(Intent.ACTION_SENDTO);
        String uriText = "mailto:" + Uri.encode("lori.duplessis@thesolutioniv.com") + "?subject=" + Uri.encode("Contact Us - Android") + "&body=" + Uri.encode(body);
        Uri uri = Uri.parse(uriText);
        send.setData(uri);
        startActivity(Intent.createChooser(send, "Send Mail"));
        toggleContactUs();
    }

    public void callSolution() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "(310) 372-7234"));
        startActivity(intent);
    }

}
