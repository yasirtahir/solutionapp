package com.solutionapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.solutionapp.R;
import com.solutionapp.Utilities.PreferenceHelper;
import com.solutionapp.retro.entities.CategoryData;
import com.solutionapp.retro.entities.CategoryEntity;
import com.solutionapp.retro.entities.ServiceData;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.retro.model.WebServiceFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;

@ContentView(R.layout.activity_splash)
public class SplashActivity extends RoboFragmentActivity {

    private PreferenceHelper preferenceHelper;

    private void showMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(getApplicationContext());
        getData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initActivity();
            }
        }, 5000);
    }

    private void initActivity() {
        showMainActivity();
    }

    private void getData() {
        // Get Categories
        WebServiceFactory.getInstance().getCategories(new Callback<Response>() {
            @Override
            public void success(Response string, Response response) {
                try {
                    JSONObject jsonObj = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    JSONObject object1 = jsonObj.getJSONObject("1");
                    JSONObject object2 = jsonObj.getJSONObject("3");
                    JSONObject object3 = jsonObj.getJSONObject("5");
                    JSONObject object4 = jsonObj.getJSONObject("7");

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    jsonObjectArrayList.add(object1);
                    jsonObjectArrayList.add(object2);
                    jsonObjectArrayList.add(object3);
                    jsonObjectArrayList.add(object4);

                    ArrayList<CategoryEntity> arrayList = new ArrayList<>();
                    for (int i = 0; i < jsonObjectArrayList.size(); i++) {
                        JSONObject jsonObject = jsonObjectArrayList.get(i);
                        CategoryEntity entity = new CategoryEntity();
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        entity.setName(name);
                        entity.setDescription(description);

                        JSONObject data = jsonObject.getJSONObject("Data");
                        CategoryData categoryData = new CategoryData();

                        String categoryImage = "";
                        String iconImage = "";
                        String featureImage = "";
                        String hexColor = "";
                        String service_categories_faq = "";
                        JSONArray servicesJsonArray = null;
                        try {
                            categoryImage = data.getJSONArray("category_image").get(0).toString();
                            iconImage = data.getJSONArray("icon_image").get(0).toString();
                            featureImage = data.getJSONArray("feature_image").get(0).toString();
                            hexColor = data.getJSONObject("terms").getString("service_categories_hex");
                            service_categories_faq = data.getJSONObject("terms").getString("service_categories_faq");
                            servicesJsonArray = data.getJSONArray("mb_services");
                        } catch (Throwable t) {
                            // Placed this try catch so that the super try catch won't get break
                        }
                        categoryData.setCategoryImage(categoryImage);
                        categoryData.setIconImage(iconImage);
                        categoryData.setFeatureImage(featureImage);
                        categoryData.setHexColor(hexColor);
                        categoryData.setService_categories_faq(service_categories_faq);

                        if(servicesJsonArray != null){
                            ArrayList<ServiceEntity> serviceArrayList = new ArrayList<>();
                            for (int j = 0; j < servicesJsonArray.length(); j++) {
                                JSONObject serviceJsonObject = servicesJsonArray.getJSONObject(j);
                                ServiceEntity serviceEntity = new ServiceEntity();
                                String postContent = serviceJsonObject.getString("post_content");
                                String post_title = serviceJsonObject.getString("post_title");
                                serviceEntity.setPostContent(postContent);
                                JSONObject serviceJsonData = serviceJsonObject.getJSONObject("Data");
                                ServiceData serviceData = new ServiceData();
                                String mb_service_price = "";
                                String featured_image = "";
                                try {
                                    mb_service_price = serviceJsonData.getJSONArray("mb_service_price").get(0).toString();
                                    featured_image = serviceJsonData.getString("featured_image");
                                } catch (Throwable t) {
                                    // Placed this try catch so that the super try catch won't get break
                                }
                                serviceData.setMbServicePrice(mb_service_price);
                                serviceData.setFeatured_image(featured_image);
                                serviceEntity.setData(serviceData);
                                serviceEntity.setPostTitle(post_title);
                                serviceArrayList.add(serviceEntity);
                            }
                            categoryData.setMb_services(serviceArrayList);
                        }
                        entity.setCategoryData(categoryData);
                        arrayList.add(entity);
                    }

                    if (arrayList.size() > 0) {
                        preferenceHelper.putCategories(arrayList);
                    }
                } catch (Throwable e) {
                    // Don't need to print this catch
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });
    }
}