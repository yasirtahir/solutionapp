package com.solutionapp.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;

import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.Utilities.utils.LoadingListener;
import com.solutionapp.R;
import com.solutionapp.Utilities.UIHelper;
import com.solutionapp.fragments.HomeFragment;
import com.solutionapp.ui.dialogs.DialogFactory;
import com.solutionapp.ui.views.TitleBar;

import roboguice.activity.RoboFragmentActivity;

/**
 * This class is marked abstract so that it can pair with Dockable Fragments
 * only. All Classes extending this will inherit this functionality of
 * interaction with menus.
 */
public abstract class DockActivity extends RoboFragmentActivity implements
        LoadingListener {

    public static final String KEY_FRAG_FIRST = "firstFrag";

    public abstract int getDockFrameLayoutId();

    protected BaseFragment baseFragment;
    private TitleBar titleBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
    }

    public void addFragment(BaseFragment frag, String tag) {
        if (isLoading()) {
            UIHelper.showShortToastInCenter(getApplicationContext(), R.string.message_wait);
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            baseFragment = frag;
            transaction.replace(getDockFrameLayoutId(), frag, tag);
            transaction.addToBackStack(getSupportFragmentManager().getBackStackEntryCount() == 0 ? KEY_FRAG_FIRST : null).commit();
        }
    }

    public void addFragmentWithAnimation(BaseFragment frag, String tag) {
        if (isLoading()) {
            UIHelper.showShortToastInCenter(getApplicationContext(), R.string.message_wait);
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            baseFragment = frag;
            transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out);
            transaction.replace(getDockFrameLayoutId(), frag, tag);
            transaction.addToBackStack(getSupportFragmentManager().getBackStackEntryCount() == 0 ? KEY_FRAG_FIRST : null).commit();
        }
    }

    public void addFragmentWithAnimation(Fragment frag, String tag) {
        if (isLoading()) {
            UIHelper.showShortToastInCenter(getApplicationContext(), R.string.message_wait);
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.push_right_in, R.anim.push_right_out, R.anim.push_left_in, R.anim.push_left_out);
            transaction.replace(getDockFrameLayoutId(), frag, tag);
            transaction.addToBackStack(getSupportFragmentManager().getBackStackEntryCount() == 0 ? KEY_FRAG_FIRST : null).commit();
        }
    }

    public void addAndShowDialogFragment(DialogFragment dialog, String tag) {
        if (isLoading()) {
            UIHelper.showShortToastInCenter(getApplicationContext(), R.string.message_wait);
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            dialog.show(transaction, tag);
        }
    }

    public void prepareAndShowDialog(DialogFragment frag, String TAG, BaseFragment fragment) {
        FragmentTransaction transaction = fragment.getChildFragmentManager().beginTransaction();
        Fragment prev = fragment.getChildFragmentManager().findFragmentByTag(TAG);
        if (prev != null)
            transaction.remove(prev);
        transaction.addToBackStack(null);
        frag.show(transaction, TAG);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                super.onBackPressed();
        } else {
            DialogFactory.createQuitDialog(this,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DockActivity.this.finish();
                        }
                    }, R.string.message_quit).show();
        }
    }

    public BaseFragment getLastAddedFragment() {
        return baseFragment;
    }

    public void emptyBackStack() {
        popBackStackTillEntry(0);
    }

    public void popBackStackTillEntry(int entryIndex) {
        if (getSupportFragmentManager() == null)
            return;
        if (getSupportFragmentManager().getBackStackEntryCount() <= entryIndex)
            return;
        BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                entryIndex);
        if (entry != null) {
            getSupportFragmentManager().popBackStack(entry.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void popFragment() {
        if (getSupportFragmentManager() == null)
            return;
        getSupportFragmentManager().popBackStack();
    }

    public boolean isFragmentPresentByString(String tag) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(tag);
        if (frag instanceof HomeFragment) {
            return true;
        } else
            return false;
    }

    private boolean isLoading() {
        titleBar = new TitleBar(DockActivity.this);
        if (titleBar.isProgressBarShowing())
            return true;
        return false;
    }

}
