
package com.solutionapp.retro.entities;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationEntity {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("title")
    @Expose
    private Title title;

    @SerializedName("loc_address1")
    @Expose
    private List<String> locAddress1 = new ArrayList<String>();

    @SerializedName("loc_address2")
    @Expose
    private List<String> locAddress2 = new ArrayList<String>();

    @SerializedName("loc_state")
    @Expose
    private List<String> locState = new ArrayList<String>();

    @SerializedName("loc_zipcode")
    @Expose
    private List<String> locZipcode = new ArrayList<String>();

    @SerializedName("loc_email")
    @Expose
    private List<String> locEmail = new ArrayList<String>();

    @SerializedName("loc_lat")
    @Expose
    private List<String> locLat = new ArrayList<String>();

    @SerializedName("loc_long")
    @Expose
    private List<String> locLong = new ArrayList<String>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    public Title getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(Title title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The locAddress1
     */
    public List<String> getLocAddress1() {
        return locAddress1;
    }

    /**
     * 
     * @param locAddress1
     *     The loc_address1
     */
    public void setLocAddress1(List<String> locAddress1) {
        this.locAddress1 = locAddress1;
    }

    /**
     * 
     * @return
     *     The locAddress2
     */
    public List<String> getLocAddress2() {
        return locAddress2;
    }

    /**
     * 
     * @param locAddress2
     *     The loc_address2
     */
    public void setLocAddress2(List<String> locAddress2) {
        this.locAddress2 = locAddress2;
    }

    /**
     * 
     * @return
     *     The locState
     */
    public List<String> getLocState() {
        return locState;
    }

    /**
     * 
     * @param locState
     *     The loc_state
     */
    public void setLocState(List<String> locState) {
        this.locState = locState;
    }

    /**
     * 
     * @return
     *     The locZipcode
     */
    public List<String> getLocZipcode() {
        return locZipcode;
    }

    /**
     * 
     * @param locZipcode
     *     The loc_zipcode
     */
    public void setLocZipcode(List<String> locZipcode) {
        this.locZipcode = locZipcode;
    }

    /**
     * 
     * @return
     *     The locEmail
     */
    public List<String> getLocEmail() {
        return locEmail;
    }

    /**
     * 
     * @param locEmail
     *     The loc_email
     */
    public void setLocEmail(List<String> locEmail) {
        this.locEmail = locEmail;
    }

    /**
     * 
     * @return
     *     The locLat
     */
    public List<String> getLocLat() {
        return locLat;
    }

    /**
     * 
     * @param locLat
     *     The loc_lat
     */
    public void setLocLat(List<String> locLat) {
        this.locLat = locLat;
    }

    /**
     * 
     * @return
     *     The locLong
     */
    public List<String> getLocLong() {
        return locLong;
    }

    /**
     * 
     * @param locLong
     *     The loc_long
     */
    public void setLocLong(List<String> locLong) {
        this.locLong = locLong;
    }

}
