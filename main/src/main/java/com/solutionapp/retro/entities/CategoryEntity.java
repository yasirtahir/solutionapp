package com.solutionapp.retro.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryEntity {

    @SerializedName("term_id")
    @Expose
    private String termId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("slug")
    @Expose
    private String slug;

    @SerializedName("term_group")
    @Expose
    private String termGroup;

    @SerializedName("term_taxonomy_id")
    @Expose
    private String termTaxonomyId;

    @SerializedName("taxonomy")
    @Expose
    private String taxonomy;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("parent")
    @Expose
    private String parent;

    @SerializedName("count")
    @Expose
    private String count;

    @SerializedName("Data")
    @Expose
    private CategoryData CategoryData;

    /**
     * 
     * @return
     *     The termId
     */
    public String getTermId() {
        return termId;
    }

    /**
     * 
     * @param termId
     *     The term_id
     */
    public void setTermId(String termId) {
        this.termId = termId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     * 
     * @param slug
     *     The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     * 
     * @return
     *     The termGroup
     */
    public String getTermGroup() {
        return termGroup;
    }

    /**
     * 
     * @param termGroup
     *     The term_group
     */
    public void setTermGroup(String termGroup) {
        this.termGroup = termGroup;
    }

    /**
     * 
     * @return
     *     The termTaxonomyId
     */
    public String getTermTaxonomyId() {
        return termTaxonomyId;
    }

    /**
     * 
     * @param termTaxonomyId
     *     The term_taxonomy_id
     */
    public void setTermTaxonomyId(String termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    /**
     * 
     * @return
     *     The taxonomy
     */
    public String getTaxonomy() {
        return taxonomy;
    }

    /**
     * 
     * @param taxonomy
     *     The taxonomy
     */
    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The parent
     */
    public String getParent() {
        return parent;
    }

    /**
     * 
     * @param parent
     *     The parent
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * 
     * @return
     *     The count
     */
    public String getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The CategoryData
     */
    public CategoryData getCategoryData() {
        return CategoryData;
    }

    /**
     * 
     * @param CategoryData
     *     The CategoryData
     */
    public void setCategoryData(CategoryData CategoryData) {
        this.CategoryData = CategoryData;
    }

}
