package com.solutionapp.retro.entities;


import java.util.ArrayList;

public class CategoryWrapper {

    ArrayList<CategoryEntity> categoryEntities = new ArrayList<>();


    public ArrayList<CategoryEntity> getCategoryEntities() {
        return categoryEntities;
    }

    public void setCategoryEntities(ArrayList<CategoryEntity> categoryEntities) {
        this.categoryEntities = categoryEntities;
    }
}
