package com.solutionapp.retro.entities;


import java.util.ArrayList;

public class ServiceWrapper {

    ArrayList<ServiceEntity> serviceEntities = new ArrayList<>();

    public ArrayList<ServiceEntity> getServiceEntities() {
        return serviceEntities;
    }

    public void setServiceEntities(ArrayList<ServiceEntity> serviceEntities) {
        this.serviceEntities = serviceEntities;
    }
}
