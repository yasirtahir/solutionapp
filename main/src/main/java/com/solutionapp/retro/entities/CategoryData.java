package com.solutionapp.retro.entities;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryData {

    @SerializedName("category_image")
    @Expose
    private String categoryImage;

    @SerializedName("icon_image")
    @Expose
    private String iconImage;

    @SerializedName("feature_image")
    @Expose
    private String featureImage;

    @SerializedName("mb_services")
    @Expose
    private ArrayList<ServiceEntity> mb_services;

    private String hexColor;
    private String service_categories_faq;

    public String getService_categories_faq() {
        return service_categories_faq;
    }

    public void setService_categories_faq(String service_categories_faq) {
        this.service_categories_faq = service_categories_faq;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

    public ArrayList<ServiceEntity> getMb_services() {
        return mb_services;
    }

    public void setMb_services(ArrayList<ServiceEntity> mb_services) {
        this.mb_services = mb_services;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getIconImage() {
        return iconImage;
    }

    public void setIconImage(String iconImage) {
        this.iconImage = iconImage;
    }

    public String getFeatureImage() {
        return featureImage;
    }

    public void setFeatureImage(String featureImage) {
        this.featureImage = featureImage;
    }
}
