package com.solutionapp.retro.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created on 11/11/2015.
 */
public class FAQWrapper {

    @SerializedName("term_id")
    @Expose
    private String term_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("term_taxonomy_id")
    @Expose
    private String term_taxonomy_id;

    @SerializedName("Data")
    @Expose
    private FAQDataWrapper Data;

    public String getTerm_id() {
        return term_id;
    }

    public void setTerm_id(String term_id) {
        this.term_id = term_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTerm_taxonomy_id() {
        return term_taxonomy_id;
    }

    public void setTerm_taxonomy_id(String term_taxonomy_id) {
        this.term_taxonomy_id = term_taxonomy_id;
    }

    public FAQDataWrapper getData() {
        return Data;
    }

    public void setData(FAQDataWrapper data) {
        Data = data;
    }
}
