package com.solutionapp.retro.entities;

import java.util.ArrayList;

/**
 * Created on 1/17/2016.
 */
public class LocationWrapper {

    ArrayList<LocationEntity> locationEntityArrayList = new ArrayList<>();

    public ArrayList<LocationEntity> getLocationEntityArrayList() {
        return locationEntityArrayList;
    }

    public void setLocationEntityArrayList(ArrayList<LocationEntity> locationEntityArrayList) {
        this.locationEntityArrayList = locationEntityArrayList;
    }
}
