package com.solutionapp.retro.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created on 1/7/2016.
 */
public class FAQDataWrapper {

    @SerializedName("faq_content")
    @Expose
    private ArrayList<FAQEntity> faq_content = new ArrayList<>();

    public ArrayList<FAQEntity> getFaq_content() {
        return faq_content;
    }

    public void setFaq_content(ArrayList<FAQEntity> faq_content) {
        this.faq_content = faq_content;
    }
}
