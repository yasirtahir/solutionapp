package com.solutionapp.retro.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 1/7/2016.
 */
public class FAQData {

    @SerializedName("question")
    @Expose
    private List<String> question = new ArrayList<String>();

    @SerializedName("answer")
    @Expose
    private List<String> answer = new ArrayList<String>();

    public List<String> getQuestion() {
        return question;
    }

    public void setQuestion(List<String> question) {
        this.question = question;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }
}
