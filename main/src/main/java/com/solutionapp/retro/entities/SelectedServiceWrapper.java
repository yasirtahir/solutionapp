package com.solutionapp.retro.entities;

import java.util.ArrayList;

/**
 * Created on 1/23/2016.
 */
public class SelectedServiceWrapper {

    private ArrayList<SelectedService> selectedServices = new ArrayList<>();

    public ArrayList<SelectedService> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(ArrayList<SelectedService> selectedServices) {
        this.selectedServices = selectedServices;
    }
}
