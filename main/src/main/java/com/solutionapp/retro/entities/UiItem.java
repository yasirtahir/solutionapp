package com.solutionapp.retro.entities;

import java.util.ArrayList;

public class UiItem {
    private String title;
    private String desc;
    private String imgUrl = "";
    private String imgIcon = "";
    private String dateCreated;
    private int resourceId;
    private String contentId = "";
    private boolean isSelected = false;
    private ArrayList<UiItem> timing = new ArrayList<>();

    public ArrayList<UiItem> getTiming() {
        return timing;
    }

    public void setTiming(ArrayList<UiItem> timing) {
        this.timing = timing;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(String imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int id) {
        this.resourceId = id;
    }
}
