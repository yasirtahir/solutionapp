package com.solutionapp.retro.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 1/7/2016.
 */
public class FAQResponse {

    @SerializedName("0")
    @Expose
    private FAQWrapper faqWrapper0;

    @SerializedName("1")
    @Expose
    private FAQWrapper faqWrapper1;

    @SerializedName("2")
    @Expose
    private FAQWrapper faqWrapper2;

    @SerializedName("3")
    @Expose
    private FAQWrapper faqWrapper3;

    @SerializedName("5")
    @Expose
    private FAQWrapper faqWrapper5;

    public FAQWrapper getFaqWrapper0() {
        return faqWrapper0;
    }

    public void setFaqWrapper0(FAQWrapper faqWrapper0) {
        this.faqWrapper0 = faqWrapper0;
    }

    public FAQWrapper getFaqWrapper1() {
        return faqWrapper1;
    }

    public void setFaqWrapper1(FAQWrapper faqWrapper1) {
        this.faqWrapper1 = faqWrapper1;
    }

    public FAQWrapper getFaqWrapper2() {
        return faqWrapper2;
    }

    public void setFaqWrapper2(FAQWrapper faqWrapper2) {
        this.faqWrapper2 = faqWrapper2;
    }

    public FAQWrapper getFaqWrapper3() {
        return faqWrapper3;
    }

    public void setFaqWrapper3(FAQWrapper faqWrapper3) {
        this.faqWrapper3 = faqWrapper3;
    }

    public FAQWrapper getFaqWrapper5() {
        return faqWrapper5;
    }

    public void setFaqWrapper5(FAQWrapper faqWrapper5) {
        this.faqWrapper5 = faqWrapper5;
    }
}
