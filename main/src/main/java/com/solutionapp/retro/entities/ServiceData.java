
package com.solutionapp.retro.entities;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceData {

    @SerializedName("mb_service_defaulttimelength")
    @Expose
    private ArrayList<String> mbServiceDefaulttimelength = new ArrayList<String>();
    @SerializedName("mb_service_numdeducted")
    @Expose
    private ArrayList<String> mbServiceNumdeducted = new ArrayList<String>();
    @SerializedName("mb_service_id")
    @Expose
    private ArrayList<String> mbServiceId = new ArrayList<String>();
    @SerializedName("_edit_lock")
    @Expose
    private ArrayList<String> EditLock = new ArrayList<String>();
    @SerializedName("_thumbnail_id")
    @Expose
    private ArrayList<String> ThumbnailId = new ArrayList<String>();
    @SerializedName("_edit_last")
    @Expose
    private ArrayList<String> EditLast = new ArrayList<String>();
    @SerializedName("mb_service_class_nonce")
    @Expose
    private ArrayList<String> mbServiceClassNonce = new ArrayList<String>();
    @SerializedName("mb_service_price")
    @Expose
    private String mbServicePrice;
    @SerializedName("featured_image")
    @Expose
    private String featured_image;

    public String getFeatured_image() {
        return featured_image;
    }

    public void setFeatured_image(String featured_image) {
        this.featured_image = featured_image;
    }

    public ArrayList<String> getMbServiceDefaulttimelength() {
        return mbServiceDefaulttimelength;
    }

    public void setMbServiceDefaulttimelength(ArrayList<String> mbServiceDefaulttimelength) {
        this.mbServiceDefaulttimelength = mbServiceDefaulttimelength;
    }

    public ArrayList<String> getMbServiceNumdeducted() {
        return mbServiceNumdeducted;
    }

    public void setMbServiceNumdeducted(ArrayList<String> mbServiceNumdeducted) {
        this.mbServiceNumdeducted = mbServiceNumdeducted;
    }

    public ArrayList<String> getMbServiceId() {
        return mbServiceId;
    }

    public void setMbServiceId(ArrayList<String> mbServiceId) {
        this.mbServiceId = mbServiceId;
    }

    public ArrayList<String> getEditLock() {
        return EditLock;
    }

    public void setEditLock(ArrayList<String> editLock) {
        EditLock = editLock;
    }

    public ArrayList<String> getThumbnailId() {
        return ThumbnailId;
    }

    public void setThumbnailId(ArrayList<String> thumbnailId) {
        ThumbnailId = thumbnailId;
    }

    public ArrayList<String> getEditLast() {
        return EditLast;
    }

    public void setEditLast(ArrayList<String> editLast) {
        EditLast = editLast;
    }

    public ArrayList<String> getMbServiceClassNonce() {
        return mbServiceClassNonce;
    }

    public void setMbServiceClassNonce(ArrayList<String> mbServiceClassNonce) {
        this.mbServiceClassNonce = mbServiceClassNonce;
    }

    public String getMbServicePrice() {
        return mbServicePrice;
    }

    public void setMbServicePrice(String mbServicePrice) {
        this.mbServicePrice = mbServicePrice;
    }
}
