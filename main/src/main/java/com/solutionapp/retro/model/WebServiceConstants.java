package com.solutionapp.retro.model;

public class WebServiceConstants {

    public static final String LOCAL_SERVER = "";
    public static final String LIVE_SERVER = "http://thesolutioniv.com/";
    public static String SERVER_URL = LIVE_SERVER;

}