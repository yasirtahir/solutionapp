package com.solutionapp.retro.model;

import com.squareup.okhttp.OkHttpClient;
import com.solutionapp.Utilities.utils.Applog;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;

public class WebServiceFactory {

    private static WebService mService;

    public static WebService getInstance() {
        if (mService == null) {
            OkHttpClient name = new OkHttpClient();
            name.setReadTimeout(60, TimeUnit.SECONDS);
            name.setConnectTimeout(61, TimeUnit.SECONDS);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(WebServiceConstants.SERVER_URL)
                    .setLogLevel(LogLevel.FULL)
                    .setClient(new OkClient(name))
                    .setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String msg) {
                            Applog.Debug("Web Response =  " + msg);
                        }
                    })
                    .setConverter(new GsonConverter(GsonFactory.getConfiguredGson()))
                    .build();
            mService = restAdapter.create(WebService.class);
        }
        return mService;
    }
}
