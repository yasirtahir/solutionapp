package com.solutionapp.retro.model;

import com.solutionapp.retro.entities.CategoryEntity;
import com.solutionapp.retro.entities.FAQDataWrapper;
import com.solutionapp.retro.entities.FAQEntity;
import com.solutionapp.retro.entities.FAQResponse;
import com.solutionapp.retro.entities.FAQWrapper;
import com.solutionapp.retro.entities.LocationEntity;
import com.solutionapp.retro.entities.ServiceEntity;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;

public interface WebService {

    @GET("/wp-json/wp/v2/service_categories/")
    public void getCategories(
            Callback<Response> callback
    );

    @GET("/wp-json/wp/v2/locations/")
    public void getLocations(
            Callback<ArrayList<LocationEntity>> callback
    );

    @GET("/wp-json/wp/v2/faq_categories/")
    public void getFAQs(
            Callback<FAQResponse> callback
    );

}
