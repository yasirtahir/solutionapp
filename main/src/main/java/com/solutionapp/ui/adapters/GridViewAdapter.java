package com.solutionapp.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.solutionapp.R;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.ui.views.AnyTextView;

import java.util.ArrayList;

public class GridViewAdapter extends ArrayAdapter<UiItem> {

    Context context;
    int layoutResourceId;
    ArrayList<UiItem> data = new ArrayList<>();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList<UiItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.txtTime = (AnyTextView) row.findViewById(R.id.txtTime);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        UiItem entity = data.get(position);
        if (entity.isSelected()) {
            holder.txtTime.setBackgroundResource(R.drawable.btn_time_pressed);
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.txtTime.setBackgroundResource(R.drawable.btn_time_normal);
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.home_list_item_blue));
        }
        holder.txtTime.setText(entity.getTitle());
        return row;
    }

    static class ViewHolder {
        AnyTextView txtTime;
    }
}



