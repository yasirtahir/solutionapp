package com.solutionapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.solutionapp.R;

public class TitleBar extends RelativeLayout {
	
	private RelativeLayout contTitleBar;
	private TextView txtTitle;
	private ImageButton btnLeft;
	private ImageButton btnRight;
    private ProgressWheel progressCall;
	private ImageView imgLogo;
	private AnyTextView txtRightHeading;
	private Button imgArrow, imgLeftArrow;

	public TitleBar( Context context ) {
		super( context );
		initLayout( context );
	}
	
	public TitleBar( Context context, AttributeSet attrs ) {
		super( context, attrs );
		initLayout( context );
		if ( attrs != null )
			initAttrs( context, attrs );
	}
	
	public TitleBar( Context context, AttributeSet attrs, int defStyle ) {
		super( context, attrs, defStyle );
		initLayout( context );
		if ( attrs != null )
			initAttrs( context, attrs );
	}
	
	private void initAttrs( Context context, AttributeSet attrs ) {
	}
	
	private void bindViews() {
		contTitleBar = (RelativeLayout) this.findViewById( R.id.header_layout );
        progressCall = (ProgressWheel) this.findViewById( R.id.progressCall );
		txtTitle = (TextView) this.findViewById( R.id.txt_subHead );
		btnRight = (ImageButton) this.findViewById( R.id.header_btn_right );
		btnLeft = (ImageButton) this.findViewById( R.id.header_btn_left );
		imgLogo = (ImageView) this.findViewById( R.id.imgLogo );
		txtRightHeading = (AnyTextView) this.findViewById( R.id.txtRightHeading );
		imgArrow = (Button) this.findViewById( R.id.imgArrow );
		imgLeftArrow = (Button) this.findViewById( R.id.imgLeftArrow );
	}
	
	private void initLayout( Context context ) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		inflater.inflate( R.layout.header_main, this );
		bindViews();
	}

	public void showTitleBar() {
		this.setVisibility(View.VISIBLE);
	}

	public void hideTitleBar() {
		this.setVisibility(View.GONE);
	}

	public void setTitleBarBackground( int color ) {
		contTitleBar.setBackgroundColor(color);
	}

	public void hideRightButton() {
		btnRight.setVisibility(View.INVISIBLE);
	}

    public void hideLeftButton() {
        btnLeft.setVisibility(View.INVISIBLE);
    }

	public void showRightButton( int drawable, OnClickListener listener ) {
		btnRight.setVisibility( View.VISIBLE );
		btnRight.setImageResource( drawable );
        btnRight.setOnClickListener(listener);
	}

    public void showLeftButton( int drawable, OnClickListener listener ) {
        btnLeft.setVisibility( View.VISIBLE );
        btnLeft.setImageResource( drawable );
        btnLeft.setOnClickListener(listener);
    }

	public void setHeaderBackground( int drawable ) {
		contTitleBar.setBackgroundResource(drawable);
	}

	public void setSubHeading( String heading ) {
		txtTitle.setVisibility( View.VISIBLE );
		txtTitle.setText(heading);
		imgLogo.setVisibility(GONE);
		txtRightHeading.setVisibility(GONE);
	}

    public void showProgressBar () {
        this.progressCall.setVisibility(View.VISIBLE);
    }

    public boolean isProgressBarShowing(){
        if(this.progressCall.getVisibility() == View.VISIBLE)
            return true;
        else
            return false;
    }

    public void hideProgressBar () {
        this.progressCall.setVisibility( INVISIBLE );
    }

    public void hideButtons(){
        hideRightButton();
        hideLeftButton();
        hideProgressBar();
		hideMenuDropDown();
		hideLeftMenuDropDown();
    }

	public void showLogo( ) {
		txtTitle.setVisibility(View.GONE);
		imgLogo.setVisibility(VISIBLE);
	}

	public void showLeftHeading(String text){
		txtTitle.setVisibility(GONE);
		imgLogo.setVisibility(GONE);
		btnLeft.setVisibility(GONE);
		txtRightHeading.setVisibility(VISIBLE);
		txtRightHeading.setText(text);
	}

	public ImageButton getRightButton(){
		return btnRight;
	}

	public ImageButton getLeftButton(){
		return btnLeft;
	}

	public void showMenuDropDown(View.OnClickListener listener){
		imgArrow.setVisibility(VISIBLE);
		imgArrow.setOnClickListener(listener);
	}

	public void hideMenuDropDown(){
		imgArrow.setVisibility(GONE);
	}

	public void showLeftMenuDropDown(View.OnClickListener listener){
		imgLeftArrow.setVisibility(VISIBLE);
		imgLeftArrow.setOnClickListener(listener);
	}

	public void hideLeftMenuDropDown(){
		imgLeftArrow.setVisibility(GONE);
	}

}
