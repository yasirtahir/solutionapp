package com.solutionapp.ui.viewbinders;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.solutionapp.R;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.ui.viewbinders.abstracts.ViewBinder;
import com.solutionapp.ui.views.AnyTextView;

/**
 * Created on 1/17/2016.
 */

public class LocationListItemBinder extends ViewBinder<UiItem> {

    public LocationListItemBinder() {
        super(R.layout.list_item_location);
    }

    @Override
    public ViewBinder.BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(UiItem entity, int position, int grpPosition, View view, Activity activity) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if(entity.isSelected()){
            holder.rlContent.setBackgroundColor(activity.getResources().getColor(R.color.home_list_item_blue));
            holder.itemName.setTextColor(activity.getResources().getColor(R.color.white));
            holder.imgArrow.setVisibility(View.VISIBLE);
        } else{
            holder.rlContent.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
            holder.itemName.setTextColor(activity.getResources().getColor(R.color.home_list_item_blue));
            holder.imgArrow.setVisibility(View.GONE);
        }
        holder.itemName.setText(entity.getTitle());
    }

    protected class ViewHolder extends ViewBinder.BaseViewHolder {

        AnyTextView itemName;
        ImageView imgArrow;
        RelativeLayout rlContent;

        public ViewHolder(View view) {
            itemName = (AnyTextView) view.findViewById(R.id.itemName);
            imgArrow = (ImageView) view.findViewById(R.id.imgArrow);
            rlContent = (RelativeLayout) view.findViewById(R.id.rlContent);
        }
    }
}
