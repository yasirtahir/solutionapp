package com.solutionapp.ui.viewbinders;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import com.solutionapp.R;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.ui.adapters.GridViewAdapter;
import com.solutionapp.ui.viewbinders.abstracts.ViewBinder;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.ExpandableHeightGridView;

/**
 * Created on 1/22/2016.
 */
public class AppointmentListItemBinder extends ViewBinder<UiItem> {

    private AdapterView.OnItemClickListener onItemClickListener;

    public AppointmentListItemBinder(AdapterView.OnItemClickListener onItemClickListener) {
        super(R.layout.list_item_appointment);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewBinder.BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(UiItem entity, int position, int grpPosition, View view, Activity activity) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.txtName.setText(entity.getTitle());
        holder.gridViewTime.setExpanded(true);
        GridViewAdapter gridViewAdapter = new GridViewAdapter(activity, R.layout.list_item_grid_view, entity.getTiming());
        holder.gridViewTime.setAdapter(gridViewAdapter);
        gridViewAdapter.notifyDataSetChanged();
        holder.gridViewTime.setOnItemClickListener(onItemClickListener);
    }

    protected class ViewHolder extends ViewBinder.BaseViewHolder {

        AnyTextView txtName;
        ExpandableHeightGridView gridViewTime;

        public ViewHolder(View view) {
            txtName = (AnyTextView) view.findViewById(R.id.txtName);
            gridViewTime = (ExpandableHeightGridView) view.findViewById(R.id.gridViewTime);
        }
    }

}
