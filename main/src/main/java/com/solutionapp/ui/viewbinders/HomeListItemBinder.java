package com.solutionapp.ui.viewbinders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.skocken.efficientadapter.lib.viewholder.EfficientViewHolder;
import com.solutionapp.R;
import com.solutionapp.Utilities.utils.Applog;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.ui.views.AnyTextView;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.text.WordUtils;

public class HomeListItemBinder extends EfficientViewHolder<UiItem> {

    public HomeListItemBinder(View itemView) {
        super(itemView);
    }

    @Override
    protected void updateView(final Context context, UiItem object) {
        final ImageView imgItemIcon = findViewByIdEfficient(R.id.imgItemIcon);
        ImageView imgItem = findViewByIdEfficient(R.id.imgItem);
        ImageView imgBottomBar = findViewByIdEfficient(R.id.imgBottomBar);
        AnyTextView txtItemName = findViewByIdEfficient(R.id.txtItemName);
        AnyTextView txtItemDesc = findViewByIdEfficient(R.id.txtItemDesc);
        txtItemName.setText(object.getTitle());
        txtItemDesc.setText(object.getDesc());
        imgBottomBar.setBackgroundColor(object.getResourceId());
        ImageLoader.getInstance().displayImage(object.getImgUrl(), imgItem);
        ImageLoader.getInstance().loadImage(object.getImgIcon(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imgItemIcon.setImageResource(context.getResources().getColor(R.color.transparent));
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imgItemIcon.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }

    @Override
    public boolean isClickable() {
        return true;
    }

}
