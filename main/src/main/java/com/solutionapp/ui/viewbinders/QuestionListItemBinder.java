package com.solutionapp.ui.viewbinders;

import android.app.Activity;
import android.view.View;

import com.solutionapp.R;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.ui.viewbinders.abstracts.ViewBinder;
import com.solutionapp.ui.views.AnyTextView;

public class QuestionListItemBinder extends ViewBinder<UiItem> {

    public QuestionListItemBinder() {
        super(R.layout.list_item_faq);
    }

    @Override
    public ViewBinder.BaseViewHolder createViewHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindView(UiItem entity, int position, int grpPosition, View view, Activity activity) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.txtQuestion.setText(entity.getTitle());
        holder.txtAnswer.setText(entity.getDesc());
    }

    protected class ViewHolder extends ViewBinder.BaseViewHolder {

        AnyTextView txtQuestion, txtAnswer;

        public ViewHolder(View view) {
            txtQuestion = (AnyTextView) view.findViewById(R.id.txtQuestion);
            txtAnswer = (AnyTextView) view.findViewById(R.id.txtAnswer);
        }
    }

}
