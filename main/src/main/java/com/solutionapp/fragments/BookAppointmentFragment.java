package com.solutionapp.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.SelectedService;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.ui.adapters.ArrayListAdapter;
import com.solutionapp.ui.viewbinders.AppointmentListItemBinder;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import roboguice.inject.InjectView;

public class BookAppointmentFragment extends BaseFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemClickListener {

    @InjectView(R.id.btnSelectService)
    private AnyTextView btnSelectService;

    @InjectView(R.id.btnSelectDate)
    private AnyTextView btnSelectDate;

    @InjectView(R.id.btnContinueBooking)
    private AnyTextView btnContinueBooking;

    @InjectView(R.id.appointmentListView)
    private ListView appointmentListView;

    private String[] services;
    private ArrayListAdapter<UiItem> mAdapter;
    private static String name = "";
    private boolean isTimeSelected = false;
    private String selectedTime = "", selectedPersonName = "";

    public static BookAppointmentFragment newInstance(String name) {
        BookAppointmentFragment.name = name;
        return new BookAppointmentFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setCurrentDate();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
            }
        }, 250);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ArrayListAdapter<>(getMainActivity(), new ArrayList<UiItem>(), new AppointmentListItemBinder(this));
    }

    private void setListeners() {
        btnSelectService.setOnClickListener(this);
        btnSelectDate.setOnClickListener(this);
        btnContinueBooking.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_purple));
        titleBar.setSubHeading(name);
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_book_appointment, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSelectService:
                selectService();
                break;
            case R.id.btnSelectDate:
                showDatePicker();
                break;
            case R.id.btnContinueBooking:
                if (checkValidity()) {
                    moveToConfirmation();
                }
                break;
        }
    }

    private void selectService() {
        if (services == null) {
            ArrayList<ServiceEntity> serviceEntities = new ArrayList<>();
            if (preferenceHelper.getServices() != null && !preferenceHelper.getServices().isEmpty())
                serviceEntities.addAll(preferenceHelper.getServices());
            services = new String[serviceEntities.size()];
            for (int i = 0; i < serviceEntities.size(); i++) {
                services[i] = serviceEntities.get(i).getPostTitle();
            }
        }
        new AlertDialog.Builder(getMainActivity())
                .setSingleChoiceItems(services, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        btnSelectService.setText(services[which]);
                        bindDataWithAdapter(getDummyItems());
                    }
                }).show();
    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        datePickerDialog.setYearRange(1910, 2036);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getChildFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        DateTime currentDate = DateTime.now().withTimeAtStartOfDay();
        String strDay = String.valueOf(day);
        String strMonth = String.valueOf(month + 1);
        if (strDay.length() == 1) {
            strDay = "0" + strDay;
        }
        if (strMonth.length() == 1) {
            strMonth = "0" + strMonth;
        }
        // Inserting the exact time in order to compare Either it's Current Date or not
        String time = currentDate.getHourOfDay() + ":" + currentDate.getMinuteOfHour() + ":" + currentDate.getSecondOfMinute();

        String pickedDate = strDay + "/" + strMonth + "/" + String.valueOf(year) + " " + time;
        DateTimeFormatter fromFormat = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        DateTime convertedTime = fromFormat.parseDateTime(pickedDate);
        if (convertedTime.isBefore(currentDate)) {
            displayWebResponse("Please select valid Date for Booking");
            btnSelectDate.setText("");
        } else {
            DateTimeFormatter toFormat = DateTimeFormat.forPattern("EEEE MMMMM dd, yyyy");
            String str = convertedTime.toString(toFormat);
            btnSelectDate.setText(str);
        }
    }

    private void setCurrentDate() {
        DateTime currentTime = DateTime.now();
        DateTimeFormatter toFormat = DateTimeFormat.forPattern("EEEE MMMMM dd, yyyy");
        String str = currentTime.toString(toFormat);
        btnSelectDate.setText(str);
    }

    private boolean checkValidity() {
        if (btnSelectService.getText().toString().trim().isEmpty()) {
            displayWebResponse("Please select Appointment Service");
            return false;
        }
        if (btnSelectDate.getText().toString().trim().isEmpty()) {
            displayWebResponse("Please select Appointment Date");
            return false;
        }
        if (!isTimeSelected) {
            displayWebResponse("Please select Appointment Time");
            return false;
        }
        return true;
    }

    private ArrayList<UiItem> getDummyTime() {
        // Timing List generation
        ArrayList<UiItem> timeArrayList = new ArrayList<>();
        timeArrayList.clear();
        UiItem time1 = new UiItem();
        time1.setTitle("9:00 am");
        time1.setIsSelected(false);

        UiItem time2 = new UiItem();
        time2.setTitle("10:00 am");
        time2.setIsSelected(false);

        UiItem time3 = new UiItem();
        time3.setTitle("11:00 am");
        time3.setIsSelected(false);

        UiItem time4 = new UiItem();
        time4.setTitle("12:00 pm");
        time4.setIsSelected(false);

        UiItem time5 = new UiItem();
        time5.setTitle("1:00 pm");
        time5.setIsSelected(false);

        UiItem time6 = new UiItem();
        time6.setTitle("2:00 pm");
        time6.setIsSelected(false);

        UiItem time7 = new UiItem();
        time7.setTitle("3:00 pm");
        time7.setIsSelected(false);

        UiItem time8 = new UiItem();
        time8.setTitle("4:00 pm");
        time8.setIsSelected(false);

        UiItem time9 = new UiItem();
        time9.setTitle("5:00 pm");
        time9.setIsSelected(false);

        timeArrayList.add(time1);
        timeArrayList.add(time2);
        timeArrayList.add(time3);
        timeArrayList.add(time4);
        timeArrayList.add(time5);
        timeArrayList.add(time6);
        timeArrayList.add(time7);
        timeArrayList.add(time8);
        timeArrayList.add(time9);

        return timeArrayList;
    }

    private ArrayList<UiItem> getDummyItems() {
        // Dummy Available Service Items
        ArrayList<UiItem> itemArrayList = new ArrayList<>();
        itemArrayList.clear();

        UiItem entity1 = new UiItem();
        entity1.setTitle("Alex Dupliss");
        entity1.setTiming(getDummyTime());

        UiItem entity2 = new UiItem();
        entity2.setTitle("Bobby Kennedy");
        entity2.setTiming(getDummyTime());

        UiItem entity3 = new UiItem();
        entity3.setTitle("John Smith");
        entity3.setTiming(getDummyTime());

        UiItem entity4 = new UiItem();
        entity4.setTitle("Martin Hector");
        entity4.setTiming(getDummyTime());

        itemArrayList.add(entity1);
        itemArrayList.add(entity2);
        itemArrayList.add(entity3);
        itemArrayList.add(entity4);

        Collections.shuffle(itemArrayList);
        return itemArrayList;
    }

    private void bindDataWithAdapter(ArrayList<UiItem> arrayList) {
        mAdapter.clearList();
        mAdapter.notifyDataSetChanged();
        if (arrayList == null || arrayList.isEmpty())
            return;
        mAdapter.addAll(arrayList);
        appointmentListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        setListViewHeight(appointmentListView);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UiItem entity = (UiItem) parent.getItemAtPosition(position);
        resetItems();
        entity.setIsSelected(true);
        mAdapter.notifyDataSetChanged();
        isTimeSelected = true;
        selectedTime = entity.getTitle();
    }

    private void resetItems() {
        for (UiItem mainItem : mAdapter.getList()) {
            for (int i = 0; i < mainItem.getTiming().size(); i++) {
                UiItem entity = mainItem.getTiming().get(i);
                entity.setIsSelected(false);
            }
        }
        isTimeSelected = false;
        selectedTime = "";
        mAdapter.notifyDataSetChanged();
    }

    private void moveToConfirmation() {
        for (UiItem mainItem : mAdapter.getList()) {
            for (int i = 0; i < mainItem.getTiming().size(); i++) {
                UiItem entity = mainItem.getTiming().get(i);
                if (entity.isSelected()) {
                    selectedPersonName = mainItem.getTitle();
                }
            }
        }
        SelectedService selectedService = new SelectedService();
        selectedService.setServiceDate(btnSelectDate.getText().toString().trim());
        selectedService.setServiceName(btnSelectService.getText().toString().trim());
        selectedService.setServiceProvider(selectedPersonName);
        selectedService.setServiceTime(selectedTime);
        selectedService.setServiceImage(getDummyServiceImage());
        getMainActivity().addFragmentWithAnimation(AppointmentConfirmationFragment.newInstance(name, selectedService), AppointmentConfirmationFragment.class.getSimpleName());
    }

    private String getDummyServiceImage() {
        ArrayList<String> imagesArray = new ArrayList<>();
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/icon-default-blue.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/iv-therapy-athlete-blue.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/booster-bcomplex.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/booster-b12.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/11/iv-therapy-sport-blue.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/iv-therapy-cold-blue.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/iv-therapy-traveler-blue.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/iv-therapy-hangover-blue.png");
        imagesArray.add("http://thesolutioniv.com/content/uploads/2015/08/iv-executive-blue.png");

        Collections.shuffle(imagesArray);
        return imagesArray.get(0);
    }
}
