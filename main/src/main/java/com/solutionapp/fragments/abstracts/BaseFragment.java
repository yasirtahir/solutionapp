package com.solutionapp.fragments.abstracts;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.andreabaccega.formedittextvalidator.Validator;
import com.solutionapp.Utilities.GPSLocationTracker;
import com.solutionapp.R;
import com.solutionapp.Utilities.PreferenceHelper;
import com.solutionapp.Utilities.UIHelper;
import com.solutionapp.activities.DockActivity;
import com.solutionapp.activities.MainActivity;
import com.solutionapp.ui.views.AnyEditTextView;
import com.solutionapp.ui.views.TitleBar;
import roboguice.fragment.RoboFragment;

public abstract class BaseFragment extends RoboFragment {

    protected Handler handler = new Handler();
    protected static DockActivity myDockActivity;
    protected PreferenceHelper preferenceHelper;
    private ProgressDialog progressDialog;
    protected GPSLocationTracker locationTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDockActivity = getDockActivity();
        preferenceHelper = new PreferenceHelper(myDockActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleBar(((MainActivity) getDockActivity()).titleBar);
        if (locationTracker == null)
            locationTracker = new GPSLocationTracker(getMainActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (locationTracker != null)
            locationTracker.stopUsingGPS();
        if (getDockActivity().getWindow() != null)
            if (getDockActivity().getWindow().getDecorView() != null)
                UIHelper.hideSoftKeyboard(getDockActivity(), getDockActivity().getWindow().getDecorView());

    }

    protected DockActivity getDockActivity() {
        DockActivity activity = (DockActivity) getActivity();
        while (activity == null) {
            activity = (DockActivity) getActivity();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return activity;
    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    protected TitleBar getTitleBar() {
        return getMainActivity().titleBar;
    }

    public void setTitleBar(TitleBar titleBar) {
        titleBar.showTitleBar();
    }

    protected void addEmptyStringValidator(AnyEditTextView... allFields) {
        for (AnyEditTextView field : allFields) {
            field.addValidator(new EmptyStringValidator());
        }
    }

    protected void notImplemented() {
        try {
            if (getMainActivity() != null) {
                UIHelper.showLongToastInCenter(getMainActivity(), getResources().getString(R.string.implement_beta));
            }
        } catch (Throwable t) {
            // Don't need to print this Catch
        }
    }

    protected void serverNotFound() {
        try {
            if (getMainActivity() != null) {
                UIHelper.showShortToastInCenter(getActivity(), getResources().getString(R.string.msg_connection_error));
            }
        } catch (Throwable t) {
            // Don't need to print this Catch
        }
    }

    protected void displayWebResponse(String msg) {
        try {
            if (getMainActivity() != null) {
                UIHelper.showShortToastInCenter(getActivity(), msg);
            }
        } catch (Throwable t) {
            // Don't need to print this Catch
        }
    }

    protected class EmptyStringValidator extends Validator {

        public EmptyStringValidator() {
            super("The field must not be empty");
        }

        @Override
        public boolean isValid(EditText et) {
            return et.getText().toString().trim().length() >= 1;
        }

    }

    protected void setListViewHeight(ListView listView) {
        try {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                return;
            }
            int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, null);
                if (listItem instanceof ViewGroup) {
                    listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                }
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.invalidate();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    protected void showLoading() {
        progressDialog = new ProgressDialog(getMainActivity());
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected void hideLoading() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
