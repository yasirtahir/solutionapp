package com.solutionapp.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import roboguice.inject.InjectView;

public class MyProfileFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.txtFullName)
    private AnyTextView txtFullName;

    @InjectView(R.id.txtEmailAddress)
    private AnyTextView txtEmailAddress;

    @InjectView(R.id.txtHomePhone)
    private AnyTextView txtHomePhone;

    @InjectView(R.id.txtCellPhone)
    private AnyTextView txtCellPhone;

    @InjectView(R.id.txtAddress1)
    private AnyTextView txtAddress1;

    @InjectView(R.id.txtAddress2)
    private AnyTextView txtAddress2;

    @InjectView(R.id.txtGender)
    private AnyTextView txtGender;

    @InjectView(R.id.txtBirthday)
    private AnyTextView txtBirthday;

    @InjectView(R.id.txtPassword)
    private AnyTextView txtPassword;

    @InjectView(R.id.btnLogOut)
    private AnyTextView btnLogOut;

    @InjectView(R.id.btnEditInformation)
    private AnyTextView btnEditInformation;

    public static MyProfileFragment newInstance() {
        return new MyProfileFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
            }
        }, 100);
    }

    private void setListeners() {
        btnEditInformation.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_profile, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEditInformation:
                getMainActivity().addFragmentWithAnimation(EditProfileFragment.newInstance(), EditProfileFragment.class.getSimpleName());
                break;
        }
    }
}

