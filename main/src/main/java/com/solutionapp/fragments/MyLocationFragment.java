package com.solutionapp.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.NYXDigital.NiceSupportMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.base.Strings;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.LocationEntity;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.retro.model.WebServiceFactory;
import com.solutionapp.ui.adapters.ArrayListAdapter;
import com.solutionapp.ui.viewbinders.LocationListItemBinder;
import com.solutionapp.ui.views.TitleBar;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import roboguice.inject.InjectView;

public class MyLocationFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.locationListView)
    private ListView locationListView;

    private ArrayListAdapter<UiItem> mAdapter;
    private static final String MAP_FRAGMENT_ID = "LocationFragment";
    private static final int ZOOM_LEVEL = 8;
    private NiceSupportMapFragment mMapFragment;

    public static MyLocationFragment newInstance() {
        return new MyLocationFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Separate Handler for Map Init
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initMap();
                setListeners();
            }
        }, 250);
        // Separate Handler for Data Values
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (preferenceHelper.getLocations() != null && preferenceHelper.getLocations().size() > 0) {
                    bindDataWithAdapter(getItems());
                } else {
                    getLocations();
                }
            }
        }, 300);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ArrayListAdapter<>(getMainActivity(), new ArrayList<UiItem>(), new LocationListItemBinder());
    }

    private void initMap() {
        if (mMapFragment == null)
            mMapFragment = new NiceSupportMapFragment();
        if (getChildFragmentManager().findFragmentByTag(MAP_FRAGMENT_ID) != null)
            return;
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        getChildFragmentManager().popBackStack();
        fragmentTransaction.add(R.id.locationMapFragment, mMapFragment, MAP_FRAGMENT_ID);
        fragmentTransaction.commit();
        try {
            MapsInitializer.initialize(getMainActivity());
        } catch (Throwable e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mMapFragment != null) {
                    // General Map Feature Settings
                    mMapFragment.getMap().setMyLocationEnabled(true);
                    mMapFragment.getMap().getUiSettings().setMyLocationButtonEnabled(true);
                    mMapFragment.getMap().getUiSettings().setZoomControlsEnabled(true);
                    mMapFragment.getMap().setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {
                            Context context = getMainActivity();
                            LinearLayout info = new LinearLayout(context);
                            info.setOrientation(LinearLayout.VERTICAL);
                            TextView title = new TextView(context);
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());
                            TextView snippet = new TextView(context);
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());
                            info.addView(title);
                            info.addView(snippet);
                            return info;
                        }
                    });
                }
            }
        }, 1000);
    }

    private void placeCurrentLocationMarker() {
        if (mMapFragment != null) {
            // User Current Location Pointer
            final LatLng latLong = new LatLng(locationTracker.getLatitude(), locationTracker.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions().position(latLong).title("Your Current Location");
            mMapFragment.getMap().addMarker(markerOptions);
        }
    }

    private void setListeners() {

    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_blue));
        titleBar.setSubHeading("My Location");
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_location, container, false);
    }

    @Override
    public void onClick(View v) {

    }

    private void bindDataWithAdapter(ArrayList<UiItem> arrayList) {
        mAdapter.clearList();
        mAdapter.notifyDataSetChanged();
        if (arrayList == null || arrayList.isEmpty())
            return;
        mAdapter.addAll(arrayList);
        locationListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                resetItems();
                UiItem entity = mAdapter.getItemFromList(position);
                entity.setIsSelected(!entity.isSelected());
                mAdapter.notifyDataSetChanged();
                preferenceHelper.putSelectedServices(preferenceHelper.getLocations().get(position));
                getMainActivity().setUI();
            }
        });
        setListViewHeight(locationListView);
        if (mMapFragment.getMap() != null) {
            addServicesMarkers();
        }
    }

    private void resetItems() {
        // Removing Previous Selection
        for (UiItem item : mAdapter.getList()) {
            item.setIsSelected(false);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void getLocations() {
        getTitleBar().showProgressBar();
        WebServiceFactory.getInstance().getLocations(new Callback<ArrayList<LocationEntity>>() {
            @Override
            public void success(ArrayList<LocationEntity> locationEntities, Response response) {
                getTitleBar().hideProgressBar();
                if (!isAdded())
                    return;
                if (locationEntities != null && locationEntities.size() > 0) {
                    preferenceHelper.putLocations(locationEntities);
                    bindDataWithAdapter(getItems());
                } else {
                    displayWebResponse("No Location found");
                    final LatLng latLong = new LatLng(locationTracker.getLatitude(), locationTracker.getLongitude());
                    mMapFragment.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLong, ZOOM_LEVEL));
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                getTitleBar().hideProgressBar();
                if (!isAdded())
                    return;
                serverNotFound();
                final LatLng latLong = new LatLng(locationTracker.getLatitude(), locationTracker.getLongitude());
                mMapFragment.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLong, ZOOM_LEVEL));
            }
        });
    }

    private ArrayList<UiItem> getItems() {
        ArrayList<UiItem> itemArrayList = new ArrayList<>();
        itemArrayList.clear();
        ArrayList<LocationEntity> arrayList = preferenceHelper.getLocations();
        for (int i = 0; i < arrayList.size(); i++) {
            LocationEntity entity = arrayList.get(i);
            UiItem uiItem = new UiItem();

            if (!Strings.isNullOrEmpty(entity.getTitle().getRendered())) {
                uiItem.setTitle(entity.getTitle().getRendered());
            } else
                uiItem.setTitle("Title not available");

            uiItem.setIsSelected(false);
            itemArrayList.add(uiItem);
        }
        return itemArrayList;
    }

    private void addServicesMarkers() {
        // Getting Location Details
        LatLng locationLatLng = null;
        if (preferenceHelper.getLocations() != null && preferenceHelper.getLocations().size() > 0) {
            ArrayList<LocationEntity> arrayList = preferenceHelper.getLocations();
            for (int i = 0; i < arrayList.size(); i++) {
                LocationEntity entity = arrayList.get(i);
                String description;
                if (!entity.getLocAddress1().get(0).trim().isEmpty()) {
                    description = entity.getLocAddress1().get(0) + "\n" + entity.getLocState().get(0) + ", " + entity.getLocZipcode().get(0) + "\n" + entity.getLocEmail().get(0);
                } else
                    description = entity.getLocState().get(0) + ", " + entity.getLocZipcode().get(0) + "\n" + entity.getLocEmail().get(0);
                // Adding Location Marker on the Map
                locationLatLng = new LatLng(Double.valueOf(entity.getLocLat().get(0)), Double.valueOf(entity.getLocLong().get(0)));
                MarkerOptions branchMarkerOptions = new MarkerOptions().position(locationLatLng).title(entity.getTitle().getRendered()).snippet(description.trim());
                mMapFragment.getMap().addMarker(branchMarkerOptions);
            }
            if (locationLatLng != null) {
                placeCurrentLocationMarker();
                mMapFragment.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, ZOOM_LEVEL));
            }
            // Selecting the previously selected Location
            if (preferenceHelper.getSelectedServices() != null) {
                for (UiItem item : mAdapter.getList()) {
                    if (preferenceHelper.getSelectedServices().getTitle().getRendered().equals(item.getTitle())) {
                        item.setIsSelected(true);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
