package com.solutionapp.fragments.childFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.GsonBuilder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.solutionapp.R;
import com.solutionapp.fragments.FaqFragment;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.ui.views.AnyTextView;

import roboguice.inject.InjectView;

public class ServiceViewPagerFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.imgService)
    private ImageView imgService;

    @InjectView(R.id.txtServiceName)
    private AnyTextView txtServiceName;

    @InjectView(R.id.txtLblFaq)
    private AnyTextView txtLblFaq;

    @InjectView(R.id.imgBackground)
    private ImageView imgBackground;

    @InjectView(R.id.btnFAQ)
    private RelativeLayout btnFAQ;

    private ServiceEntity entity;
    private static String name, termID;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String entity = getArguments().getString("serviceEntity");
        name = getArguments().getString("name");
        termID = getArguments().getString("termID");
        this.entity = new GsonBuilder().create().fromJson(entity, ServiceEntity.class);
        setListeners();
        setData();
    }

    private void setListeners() {
        btnFAQ.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_view_pager, container, false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnFAQ) {
            if (getMainActivity().checkRightMenu() && getMainActivity().checkLeftMenu())
                getMainActivity().addFragmentWithAnimation(FaqFragment.newInstance(name, termID), FaqFragment.class.getSimpleName());
        }
    }

    private void setData() {
        txtLblFaq.setText(getMainActivity().getResources().getString(R.string.faq_text));
        if (entity != null) {
            txtServiceName.setText(entity.getPostTitle());
            ImageLoader.getInstance().displayImage("drawable://" + R.drawable.ic_launcher, imgService);
            imgBackground.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.service_selected));
            if(entity.getData() != null && entity.getData().getFeatured_image() != null){
                ImageLoader.getInstance().displayImage(entity.getData().getFeatured_image(), imgService);
            }
        }
    }
}
