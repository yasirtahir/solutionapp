package com.solutionapp.fragments.childFragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.ui.views.AnyEditTextView;
import com.solutionapp.ui.views.AnyTextView;

import roboguice.inject.InjectView;

public class ExistingClientFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.edtTxtEmailAddress)
    private AnyEditTextView edtTxtEmailAddress;

    @InjectView(R.id.edtTxtPassword)
    private AnyEditTextView edtTxtPassword;

    @InjectView(R.id.btnLogin)
    private AnyTextView btnLogin;

    @InjectView(R.id.btnForgotPassword)
    private AnyTextView btnForgotPassword;

    public static ExistingClientFragment newInstance() {
        return new ExistingClientFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
            }
        }, 100);
    }

    private void setListeners() {
        btnLogin.setOnClickListener(this);
        btnForgotPassword.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_existing_client, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (edtTxtEmailAddress.testValidity() && edtTxtPassword.testValidity()) {
                    notImplemented();
                }
                break;
            case R.id.btnForgotPassword:
                notImplemented();
                break;
        }
    }
}

