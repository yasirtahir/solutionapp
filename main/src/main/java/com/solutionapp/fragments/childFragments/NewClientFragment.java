package com.solutionapp.fragments.childFragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.solutionapp.R;
import com.solutionapp.fragments.MyProfileFragment;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.ui.views.AnyEditTextView;
import com.solutionapp.ui.views.AnyTextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

import roboguice.inject.InjectView;

public class NewClientFragment extends BaseFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    @InjectView(R.id.edtTxtName)
    private AnyEditTextView edtTxtName;

    @InjectView(R.id.edtTxtHomePhone)
    private AnyEditTextView edtTxtHomePhone;

    @InjectView(R.id.edtTxtCellPhone)
    private AnyEditTextView edtTxtCellPhone;

    @InjectView(R.id.edtTxtAddress1)
    private AnyEditTextView edtTxtAddress1;

    @InjectView(R.id.edtTxtAddress2)
    private AnyEditTextView edtTxtAddress2;

    @InjectView(R.id.btnSelectGender)
    private AnyTextView btnSelectGender;

    @InjectView(R.id.btnSelectBirthday)
    private AnyTextView btnSelectBirthday;

    @InjectView(R.id.edtTxtEmailAddress)
    private AnyEditTextView edtTxtEmailAddress;

    @InjectView(R.id.edtTxtPassword)
    private AnyEditTextView edtTxtPassword;

    @InjectView(R.id.btnSignUp)
    private AnyTextView btnSignUp;

    public static NewClientFragment newInstance(){
        return new NewClientFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
            }
        }, 100);
    }

    private void setListeners() {
        btnSignUp.setOnClickListener(this);
        btnSelectGender.setOnClickListener(this);
        btnSelectBirthday.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_client, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignUp:
                if(checkValidity()){
                    getMainActivity().addFragmentWithAnimation(MyProfileFragment.newInstance(), MyProfileFragment.class.getSimpleName());
                }
                break;
            case R.id.btnSelectGender:
                selectGender();
                break;
            case R.id.btnSelectBirthday:
                showDatePicker();
                break;
        }
    }

    private boolean checkValidity(){
        return edtTxtName.testValidity() && edtTxtHomePhone.testValidity() && edtTxtCellPhone.testValidity() && edtTxtAddress1.testValidity() && checkGender() && checkDOB() && edtTxtEmailAddress.testValidity() && edtTxtPassword.testValidity();
    }

    private boolean checkGender(){
        if(btnSelectGender.getText().toString().isEmpty()){
            displayWebResponse("Please select Gender");
            return false;
        }
        return true;
    }

    private boolean checkDOB(){
        if(btnSelectBirthday.getText().toString().isEmpty()){
            displayWebResponse("Please select Birthday");
            return false;
        }
        return true;
    }

    private void selectGender(){
        final String[] genders = {"Male", "Female"};
        new AlertDialog.Builder(getMainActivity())
                .setSingleChoiceItems(genders, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        btnSelectGender.setText(genders[which]);
                    }
                }).show();
    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        datePickerDialog.setYearRange(1910, 2036);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getChildFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        DateTime currentDate = DateTime.now().withTimeAtStartOfDay();
        String strDay = String.valueOf(day);
        String strMonth = String.valueOf(month + 1);
        if (strDay.length() == 1) {
            strDay = "0" + strDay;
        }
        if (strMonth.length() == 1) {
            strMonth = "0" + strMonth;
        }
        // Inserting the exact time in order to compare Either it's Current Date or not
        String time = currentDate.getHourOfDay() + ":" + currentDate.getMinuteOfHour() + ":" + currentDate.getSecondOfMinute();

        String pickedDate = strDay + "/" + strMonth + "/" + String.valueOf(year) + " " + time;
        DateTimeFormatter fromFormat = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        DateTime convertedTime = fromFormat.parseDateTime(pickedDate);
        if (convertedTime.isAfter(currentDate) || convertedTime.isEqual(currentDate)) {
            displayWebResponse("Please select valid Date of Birth");
            btnSelectBirthday.setText("");
        } else {
            DateTimeFormatter toFormat = DateTimeFormat.forPattern("MMMMM dd, yyyy");
            String str = convertedTime.toString(toFormat);
            btnSelectBirthday.setText(str);
        }
    }

}


