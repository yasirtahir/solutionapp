package com.solutionapp.fragments.childFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.GsonBuilder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.SelectedService;
import com.solutionapp.ui.views.AnyTextView;

import roboguice.inject.InjectView;

/**
 * Created on 1/23/2016.
 */
public class ScheduleViewPagerFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.imgService)
    private ImageView imgService;

    @InjectView(R.id.txtServiceName)
    private AnyTextView txtServiceName;

    @InjectView(R.id.imgBackground)
    private ImageView imgBackground;

    @InjectView(R.id.txtLblFaq)
    private AnyTextView txtLblFaq;

    private SelectedService entity;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String entity = getArguments().getString("scheduledServiceEntity");
        this.entity = new GsonBuilder().create().fromJson(entity, SelectedService.class);
        setData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_view_pager, container, false);
    }

    @Override
    public void onClick(View v) {
    }

    private void setData() {
        if (entity != null) {
            txtServiceName.setText(entity.getServiceName());
            ImageLoader.getInstance().displayImage("drawable://" + R.drawable.ic_launcher, imgService);
            imgBackground.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.service_selected));
            txtLblFaq.setText("with " + entity.getServiceProvider());
            ImageLoader.getInstance().displayImage(entity.getServiceImage(), imgService);
        }
    }
}
