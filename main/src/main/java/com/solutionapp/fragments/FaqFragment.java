package com.solutionapp.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.FAQEntity;
import com.solutionapp.retro.entities.FAQResponse;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.retro.model.WebServiceFactory;
import com.solutionapp.ui.adapters.ArrayListAdapter;
import com.solutionapp.ui.viewbinders.QuestionListItemBinder;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import roboguice.inject.InjectView;

public class FaqFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.listViewFAQ)
    private ListView listViewFAQ;

    @InjectView(R.id.btnBookNow)
    private AnyTextView btnBookNow;

    @InjectView(R.id.btnComeToMe)
    private AnyTextView btnComeToMe;

    @InjectView(R.id.txtEmpty)
    private AnyTextView txtEmpty;

    @InjectView(R.id.llBottomButton)
    private LinearLayout llBottomButton;

    private static String name = "", termID = "";
    private ArrayListAdapter<UiItem> mAdapter;

    public static FaqFragment newInstance(String name, String termID) {
        FaqFragment.name = name;
        FaqFragment.termID = termID;
        return new FaqFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
                setUI();
                if (preferenceHelper.getFAQs() != null) {
                    bindDataWithAdapter(preferenceHelper.getFAQs());
                } else {
                    getFAQs();
                }
            }
        }, 250);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ArrayListAdapter<>(getMainActivity(), new ArrayList<UiItem>(), new QuestionListItemBinder());
    }

    private void setListeners() {
        btnBookNow.setOnClickListener(this);
        btnComeToMe.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_purple));
        titleBar.setSubHeading(name);
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_faqs, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBookNow:
                if (getMainActivity().checkRightMenu() && getMainActivity().checkLeftMenu())
                    getMainActivity().addFragmentWithAnimation(BookAppointmentFragment.newInstance(name), BookAppointmentFragment.class.getSimpleName());
                break;
            case R.id.btnComeToMe:
                if (getMainActivity().checkRightMenu() && getMainActivity().checkLeftMenu())
                    getMainActivity().callSolution();
                break;
        }
    }

    private void getFAQs() {
        // Get FAQs
        getTitleBar().showProgressBar();
        WebServiceFactory.getInstance().getFAQs(new Callback<FAQResponse>() {
            @Override
            public void success(FAQResponse faqResponse, Response response) {
                getTitleBar().hideProgressBar();
                if (!isAdded())
                    return;
                if (faqResponse != null) {
                    ArrayList<FAQEntity> faqEntities = new ArrayList<>();

                    for(int i = 0; i < faqResponse.getFaqWrapper0().getData().getFaq_content().size(); i++){
                        faqResponse.getFaqWrapper0().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper0().getTerm_id());
                    }
                    for(int i = 0; i < faqResponse.getFaqWrapper1().getData().getFaq_content().size(); i++){
                        faqResponse.getFaqWrapper1().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper1().getTerm_id());
                    }
                    for(int i = 0; i < faqResponse.getFaqWrapper2().getData().getFaq_content().size(); i++){
                        faqResponse.getFaqWrapper2().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper2().getTerm_id());
                    }
                    for(int i = 0; i < faqResponse.getFaqWrapper3().getData().getFaq_content().size(); i++){
                        faqResponse.getFaqWrapper3().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper3().getTerm_id());
                    }
                    for(int i = 0; i < faqResponse.getFaqWrapper5().getData().getFaq_content().size(); i++){
                        faqResponse.getFaqWrapper5().getData().getFaq_content().get(i).setTermID(faqResponse.getFaqWrapper5().getTerm_id());
                    }
                    faqEntities.addAll(faqResponse.getFaqWrapper0().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper1().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper2().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper3().getData().getFaq_content());
                    faqEntities.addAll(faqResponse.getFaqWrapper5().getData().getFaq_content());
                    preferenceHelper.putFAQs(faqEntities);
                    bindDataWithAdapter(faqEntities);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                getTitleBar().hideProgressBar();
                if (!isAdded())
                    return;
                serverNotFound();
            }
        });
    }

    private void setUI() {
        if (mAdapter.getList() != null && mAdapter.getList().size() > 0) {
            listViewFAQ.setVisibility(View.VISIBLE);
            llBottomButton.setVisibility(View.VISIBLE);
            txtEmpty.setVisibility(View.GONE);
        } else {
            listViewFAQ.setVisibility(View.GONE);
            llBottomButton.setVisibility(View.GONE);
            txtEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void bindDataWithAdapter(ArrayList<FAQEntity> arrayList) {
        mAdapter.clearList();
        mAdapter.notifyDataSetChanged();
        if (arrayList == null || arrayList.isEmpty())
            return;
        ArrayList<UiItem> items = new ArrayList<>();
        for (FAQEntity entity : arrayList) {
            if(termID.equalsIgnoreCase(entity.getTermID())){
                UiItem uiItem = new UiItem();
                if (entity.getData().getQuestion() != null && entity.getData().getQuestion().size() > 0) {
                    uiItem.setTitle(entity.getData().getQuestion().get(0));
                } else
                    uiItem.setTitle("Question is not available");

                if (entity.getData().getAnswer() != null && entity.getData().getAnswer().size() > 0) {
                    uiItem.setDesc(entity.getData().getAnswer().get(0));
                } else
                    uiItem.setImgIcon("Answer is not available");
                items.add(uiItem);
            }
        }
        mAdapter.addAll(items);
        listViewFAQ.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        setUI();
    }


}
