package com.solutionapp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Strings;
import com.skocken.efficientadapter.lib.adapter.EfficientAdapter;
import com.skocken.efficientadapter.lib.adapter.EfficientRecyclerAdapter;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.CategoryData;
import com.solutionapp.retro.entities.CategoryEntity;
import com.solutionapp.retro.entities.ServiceData;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.retro.entities.UiItem;
import com.solutionapp.retro.model.WebServiceFactory;
import com.solutionapp.ui.viewbinders.HomeListItemBinder;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import roboguice.inject.InjectView;

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.txtEmpty)
    private AnyTextView txtEmpty;

    @InjectView(R.id.homeRecyclerView)
    RecyclerView homeRecyclerView;

    private EfficientRecyclerAdapter<UiItem> mHomeAdapter;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                setUI();
                setListeners();
                // Bind Data
                if (preferenceHelper.getCategories() != null) {
                    bindDataWithAdapter(getItems());
                } else {
                    getCategories();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Creating all Adapter
        mHomeAdapter = new EfficientRecyclerAdapter<>(R.layout.list_item_home, HomeListItemBinder.class, new ArrayList<UiItem>());
    }

    private void setListeners() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getMainActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        homeRecyclerView.setLayoutManager(layoutManager);
        if (mHomeAdapter != null)
            homeRecyclerView.setAdapter(mHomeAdapter);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_blue));
        titleBar.showLogo();
        titleBar.showLeftButton(R.drawable.btn_map_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().toggleLeftMenu();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkLeftMenu())
                    getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onClick(View v) {
    }

    private void bindDataWithAdapter(ArrayList<UiItem> arrayList) {
        mHomeAdapter.clear();
        mHomeAdapter.notifyDataSetChanged();
        if (arrayList == null || arrayList.isEmpty())
            return;
        mHomeAdapter.addAll(arrayList);
        homeRecyclerView.setAdapter(mHomeAdapter);
        mHomeAdapter.notifyDataSetChanged();
        mHomeAdapter.setOnItemClickListener(new EfficientAdapter.OnItemClickListener<UiItem>() {
            @Override
            public void onItemClick(EfficientAdapter<UiItem> adapter, View view, UiItem object, int position) {
                if (getMainActivity().checkRightMenu() && getMainActivity().checkLeftMenu()){
                    preferenceHelper.putServices(preferenceHelper.getCategories().get(position).getCategoryData().getMb_services());
                    getMainActivity().addFragmentWithAnimation(ServiceSelectorFragment.newInstance(object.getTitle(), preferenceHelper.getCategories().get(position).getCategoryData().getService_categories_faq()), ServiceSelectorFragment.class.getSimpleName());
                }
            }
        });
        setUI();
    }

    private void setUI() {
        if (mHomeAdapter.getObjects() != null && mHomeAdapter.getObjects().size() > 0) {
            homeRecyclerView.setVisibility(View.VISIBLE);
            txtEmpty.setVisibility(View.GONE);
        } else {
            homeRecyclerView.setVisibility(View.GONE);
            txtEmpty.setVisibility(View.VISIBLE);
        }
    }

    private ArrayList<UiItem> getItems() {
        ArrayList<UiItem> itemArrayList = new ArrayList<>();
        itemArrayList.clear();
        ArrayList<CategoryEntity> arrayList = preferenceHelper.getCategories();
        for (int i = 0; i < arrayList.size(); i++) {
            CategoryEntity entity = arrayList.get(i);
            UiItem uiItem = new UiItem();

            if (!Strings.isNullOrEmpty(entity.getName())) {
                uiItem.setTitle(entity.getName());
            } else
                uiItem.setTitle("Title not available");

            if (!Strings.isNullOrEmpty(entity.getDescription())) {
                uiItem.setDesc(entity.getDescription());
            } else
                uiItem.setDesc("Description not available");

            if (entity.getCategoryData() != null && entity.getCategoryData().getCategoryImage() != null) {
                uiItem.setImgUrl(entity.getCategoryData().getCategoryImage());
            } else
                uiItem.setImgUrl("");

            if (entity.getCategoryData() != null && entity.getCategoryData().getIconImage() != null) {
                uiItem.setImgIcon(entity.getCategoryData().getIconImage());
            } else
                uiItem.setImgIcon("");

            if (entity.getCategoryData() != null && !entity.getCategoryData().getHexColor().isEmpty()) {
                String color = entity.getCategoryData().getHexColor();
                if(!color.contains("#")){
                    color = "#" + color;
                }
                color = color.toLowerCase();
                uiItem.setResourceId(Color.parseColor(color));
            } else {
                switch (i) {
                    case 0:
                        uiItem.setResourceId(R.color.home_list_item_blue);
                        break;
                    case 1:
                        uiItem.setResourceId(R.color.home_list_item_green);
                        break;
                    case 2:
                        uiItem.setResourceId(R.color.home_list_item_yellow);
                        break;
                    case 3:
                        uiItem.setResourceId(R.color.home_list_item_purple);
                        break;
                    default:
                        uiItem.setResourceId(R.color.home_list_item_blue);
                        break;
                }
            }
            itemArrayList.add(uiItem);
        }

        return itemArrayList;
    }

    private void getCategories() {
        // Get Categories
        getTitleBar().showProgressBar();
        WebServiceFactory.getInstance().getCategories(new Callback<Response>() {
            @Override
            public void success(Response string, Response response) {
                if (!isAdded())
                    return;
                try {
                    JSONObject jsonObj = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    JSONObject object1 = jsonObj.getJSONObject("1");
                    JSONObject object2 = jsonObj.getJSONObject("3");
                    JSONObject object3 = jsonObj.getJSONObject("5");
                    JSONObject object4 = jsonObj.getJSONObject("7");

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    jsonObjectArrayList.add(object1);
                    jsonObjectArrayList.add(object2);
                    jsonObjectArrayList.add(object3);
                    jsonObjectArrayList.add(object4);

                    ArrayList<CategoryEntity> arrayList = new ArrayList<>();
                    for (int i = 0; i < jsonObjectArrayList.size(); i++) {
                        JSONObject jsonObject = jsonObjectArrayList.get(i);
                        CategoryEntity entity = new CategoryEntity();
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        entity.setName(name);
                        entity.setDescription(description);

                        JSONObject data = jsonObject.getJSONObject("Data");
                        CategoryData categoryData = new CategoryData();

                        String categoryImage = "";
                        String iconImage = "";
                        String featureImage = "";
                        String hexColor = "";
                        String service_categories_faq = "";
                        JSONArray servicesJsonArray = null;
                        try {
                            categoryImage = data.getJSONArray("category_image").get(0).toString();
                            iconImage = data.getJSONArray("icon_image").get(0).toString();
                            featureImage = data.getJSONArray("feature_image").get(0).toString();
                            hexColor = data.getJSONObject("terms").getString("service_categories_hex");
                            service_categories_faq = data.getJSONObject("terms").getString("service_categories_faq");
                            servicesJsonArray = data.getJSONArray("mb_services");
                        } catch (Throwable t) {
                            // Placed this try catch so that the super try catch won't get break
                        }
                        categoryData.setCategoryImage(categoryImage);
                        categoryData.setIconImage(iconImage);
                        categoryData.setFeatureImage(featureImage);
                        categoryData.setHexColor(hexColor);
                        categoryData.setService_categories_faq(service_categories_faq);

                        if(servicesJsonArray != null){
                            ArrayList<ServiceEntity> serviceArrayList = new ArrayList<>();
                            for (int j = 0; j < servicesJsonArray.length(); j++) {
                                JSONObject serviceJsonObject = servicesJsonArray.getJSONObject(j);
                                ServiceEntity serviceEntity = new ServiceEntity();
                                String postContent = serviceJsonObject.getString("post_content");
                                String post_title = serviceJsonObject.getString("post_title");
                                serviceEntity.setPostContent(postContent);
                                JSONObject serviceJsonData = serviceJsonObject.getJSONObject("Data");
                                ServiceData serviceData = new ServiceData();
                                String mb_service_price = "";
                                String featured_image = "";
                                try {
                                    mb_service_price = serviceJsonData.getJSONArray("mb_service_price").get(0).toString();
                                    featured_image = serviceJsonData.getString("featured_image");
                                } catch (Throwable t) {
                                    // Placed this try catch so that the super try catch won't get break
                                }
                                serviceData.setMbServicePrice(mb_service_price);
                                serviceData.setFeatured_image(featured_image);
                                serviceEntity.setData(serviceData);
                                serviceEntity.setPostTitle(post_title);
                                serviceArrayList.add(serviceEntity);
                            }
                            categoryData.setMb_services(serviceArrayList);
                        }
                        entity.setCategoryData(categoryData);
                        arrayList.add(entity);
                    }

                    if (arrayList.size() > 0) {
                        preferenceHelper.putCategories(arrayList);
                        bindDataWithAdapter(getItems());
                    }
                    getTitleBar().hideProgressBar();
                } catch (Throwable e) {
                    e.printStackTrace();
                    getTitleBar().hideProgressBar();
                    setUI();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                getTitleBar().hideProgressBar();
                if (!isAdded())
                    return;
                setUI();
                serverNotFound();
            }
        });
    }
}
