package com.solutionapp.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.GsonBuilder;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.fragments.childFragments.ServiceViewPagerFragment;
import com.solutionapp.retro.entities.ServiceEntity;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import roboguice.inject.InjectView;

public class ServiceSelectorFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.viewPagerServices)
    private ViewPager viewPagerServices;

    @InjectView(R.id.pageIndicator)
    private CirclePageIndicator pageIndicator;

    @InjectView(R.id.txtServiceCost)
    private AnyTextView txtServiceCost;

    @InjectView(R.id.txtServiceDescription)
    private AnyTextView txtServiceDescription;

    @InjectView(R.id.btnBookNow)
    private AnyTextView btnBookNow;

    @InjectView(R.id.btnComeToMe)
    private AnyTextView btnComeToMe;

    private static String name = "", termID = "";
    private ServicePagerAdapter pagerAdapter;
    private ArrayList<ServiceEntity> arrayServiceEntities = new ArrayList<>();

    public static ServiceSelectorFragment newInstance(String name, String termID) {
        ServiceSelectorFragment.name = name;
        ServiceSelectorFragment.termID = termID;
        return new ServiceSelectorFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
                if (preferenceHelper.getServices() != null) {
                    arrayServiceEntities.clear();
                    arrayServiceEntities.addAll(preferenceHelper.getServices());
                    initViewPager();
                }
            }
        }, 200);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setListeners() {
        btnBookNow.setOnClickListener(this);
        btnComeToMe.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_purple));
        titleBar.setSubHeading(name);
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_selector, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBookNow:
                if (getMainActivity().checkRightMenu() && getMainActivity().checkLeftMenu())
                    getMainActivity().addFragmentWithAnimation(BookAppointmentFragment.newInstance(name), BookAppointmentFragment.class.getSimpleName());
                break;
            case R.id.btnComeToMe:
                if (getMainActivity().checkRightMenu() && getMainActivity().checkLeftMenu())
                    getMainActivity().callSolution();
                break;
        }
    }

    private void initViewPager() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pagerAdapter = new ServicePagerAdapter(getChildFragmentManager());
                viewPagerServices.setAdapter(pagerAdapter);
                viewPagerServices.setCurrentItem(0);
                updateSelectedService(0);
                pageIndicator.setViewPager(viewPagerServices);
                // We set this on the indicator, NOT the pager
                pageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        updateSelectedService(position);
                        pagerAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });
                viewPagerServices.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        updateSelectedService(position);
                        pagerAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        }, 100);
    }

    private class ServicePagerAdapter extends FragmentPagerAdapter {

        public ServicePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public Fragment getItem(int pos) {
            String entity = new GsonBuilder().create().toJson(arrayServiceEntities.get(pos));
            ServiceViewPagerFragment pagerFragment = new ServiceViewPagerFragment();
            Bundle extras = new Bundle();
            extras.putString("serviceEntity", entity);
            extras.putString("name", name);
            extras.putString("termID", termID);
            pagerFragment.setArguments(extras);
            return pagerFragment;
        }

        @Override
        public int getCount() {
            if (arrayServiceEntities.size() < 8) {
                return arrayServiceEntities.size();
            } else {
                return 8;
            }
        }
    }

    private void resetServices() {
        for (int i = 0; i < arrayServiceEntities.size(); i++) {
            ServiceEntity entity = arrayServiceEntities.get(i);
            entity.setIsSelected(false);
            arrayServiceEntities.remove(i);
            arrayServiceEntities.add(i, entity);
        }
    }

    private void updateSelectedService(int position) {
        resetServices();
        ServiceEntity entity = arrayServiceEntities.get(position);
        entity.setIsSelected(true);
        arrayServiceEntities.remove(position);
        arrayServiceEntities.add(position, entity);
        // Updating Other Values
        txtServiceDescription.setText(Html.fromHtml(entity.getPostContent()));
        txtServiceDescription.setMovementMethod(new ScrollingMovementMethod());
        if (entity.getData() != null && entity.getData().getMbServicePrice() != null && !entity.getData().getMbServicePrice().isEmpty()) {
            txtServiceCost.setText(entity.getData().getMbServicePrice());
        } else
            txtServiceCost.setText("");
    }

}
