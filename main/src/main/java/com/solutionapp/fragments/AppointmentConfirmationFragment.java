package com.solutionapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.retro.entities.SelectedService;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import roboguice.inject.InjectView;

public class AppointmentConfirmationFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.txtServiceName)
    private AnyTextView txtServiceName;

    @InjectView(R.id.txtServiceProviderName)
    private AnyTextView txtServiceProviderName;

    @InjectView(R.id.txtLocation)
    private AnyTextView txtLocation;

    @InjectView(R.id.txtTime)
    private AnyTextView txtTime;

    @InjectView(R.id.btnContinueBooking)
    private AnyTextView btnContinueBooking;

    @InjectView(R.id.btnCancelBooking)
    private AnyTextView btnCancelBooking;

    @InjectView(R.id.rlConfirmationButtons)
    private RelativeLayout rlConfirmationButtons;

    @InjectView(R.id.btnAddToCalendar)
    private AnyTextView btnAddToCalendar;

    @InjectView(R.id.btnDone)
    private AnyTextView btnDone;

    @InjectView(R.id.llBottomButton)
    private LinearLayout llBottomButton;

    @InjectView(R.id.txtLblAppointment)
    private AnyTextView txtLblAppointment;

    private static String name;
    private static SelectedService service;

    public static AppointmentConfirmationFragment newInstance(String name, SelectedService service) {
        AppointmentConfirmationFragment.name = name;
        AppointmentConfirmationFragment.service = service;
        return new AppointmentConfirmationFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
                setUI();
            }
        }, 250);
    }

    private void setUI() {
        rlConfirmationButtons.setVisibility(View.VISIBLE);
        llBottomButton.setVisibility(View.INVISIBLE);
        txtLblAppointment.setText(getMainActivity().getResources().getString(R.string.appointment_confirmation_text));
        if (service != null) {
            txtServiceName.setText(service.getServiceName());
            txtServiceProviderName.setText("with " + service.getServiceProvider());
            txtTime.setText(service.getServiceDate() + " @ " + service.getServiceTime());
            if (preferenceHelper.getSelectedServices() != null) {
                txtLocation.setText(preferenceHelper.getSelectedServices().getTitle().getRendered());
            } else {
                txtLocation.setText("No Location selected");
            }
        }
    }

    private void setListeners() {
        btnDone.setOnClickListener(this);
        btnAddToCalendar.setOnClickListener(this);
        btnCancelBooking.setOnClickListener(this);
        btnContinueBooking.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_purple));
        titleBar.setSubHeading(name);
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_appointment_confirmation, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddToCalendar:
                addToCalendar();
                break;
            case R.id.btnCancelBooking:
                getMainActivity().popFragment();
                break;
            case R.id.btnContinueBooking:
                rlConfirmationButtons.setVisibility(View.INVISIBLE);
                llBottomButton.setVisibility(View.VISIBLE);
                txtLblAppointment.setText(getMainActivity().getResources().getString(R.string.appointment_confirmation));
                ArrayList<SelectedService> arrayList = new ArrayList<>();
                if (preferenceHelper.getScheduledServices() != null) {
                    arrayList = preferenceHelper.getScheduledServices();
                }
                arrayList.add(service);
                preferenceHelper.putScheduledServices(arrayList);
                break;
            case R.id.btnDone:
                getMainActivity().popBackStackTillEntry(1);
                break;
        }
    }

    private void addToCalendar() {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new SimpleDateFormat("EEEE MMMMM dd, yyyy").parse(service.getServiceDate()));
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setData(CalendarContract.Events.CONTENT_URI);
            intent.putExtra("beginTime", cal.getTimeInMillis());
            intent.putExtra("allDay", true);
            intent.putExtra("rrule", "FREQ=YEARLY");
            intent.putExtra("endTime", cal.getTimeInMillis() + 60 * 60 * 1000);
            intent.putExtra("title", "Your Appointment for " + service.getServiceName() + " with " + service.getServiceProvider() + " is scheduled.");
            startActivity(intent);
        } catch (ParseException e) {
            e.printStackTrace();
            displayWebResponse("Unable to add this to calendar");
        }
    }

}
