package com.solutionapp.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.fragments.childFragments.ExistingClientFragment;
import com.solutionapp.fragments.childFragments.NewClientFragment;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;

import roboguice.inject.InjectView;

public class MyAccountParentFragment extends BaseFragment implements View.OnClickListener {

   @InjectView(R.id.btnExisting)
   private AnyTextView btnExisting;

   @InjectView(R.id.btnNewClient)
   private AnyTextView btnNewClient;

    public static MyAccountParentFragment newInstance() {
        return new MyAccountParentFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
                replaceChildFragment(ExistingClientFragment.newInstance());
            }
        }, 250);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setListeners() {
        btnExisting.setOnClickListener(this);
        btnNewClient.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_blue));
        titleBar.setSubHeading("My Account");
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account_parent, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnExisting:
                btnExisting.setBackgroundResource(R.color.white);
                btnNewClient.setBackgroundResource(R.color.tab_not_selected_grey);
                replaceChildFragment(ExistingClientFragment.newInstance());
                break;
            case R.id.btnNewClient:
                btnExisting.setBackgroundResource(R.color.tab_not_selected_grey);
                btnNewClient.setBackgroundResource(R.color.white);
                replaceChildFragment(NewClientFragment.newInstance());
                break;
        }
    }

    private void replaceChildFragment(Fragment fragment){
        getChildFragmentManager().beginTransaction().replace(R.id.flAccount, fragment).commit();
    }
}
