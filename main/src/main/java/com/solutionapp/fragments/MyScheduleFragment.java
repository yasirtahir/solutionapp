package com.solutionapp.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.GsonBuilder;
import com.solutionapp.R;
import com.solutionapp.fragments.abstracts.BaseFragment;
import com.solutionapp.fragments.childFragments.ScheduleViewPagerFragment;
import com.solutionapp.retro.entities.SelectedService;
import com.solutionapp.ui.dialogs.DialogFactory;
import com.solutionapp.ui.views.AnyTextView;
import com.solutionapp.ui.views.TitleBar;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import roboguice.inject.InjectView;

/**
 * Created on 1/23/2016.
 */
public class MyScheduleFragment extends BaseFragment implements View.OnClickListener {

    @InjectView(R.id.txtLocation)
    private AnyTextView txtLocation;

    @InjectView(R.id.txtTime)
    private AnyTextView txtTime;

    @InjectView(R.id.btnUberPickUp)
    private AnyTextView btnUberPickUp;

    @InjectView(R.id.btnCancel)
    private AnyTextView btnCancel;

    @InjectView(R.id.viewPagerSchedule)
    private ViewPager viewPagerServices;

    @InjectView(R.id.llBottomButton)
    private LinearLayout llBottomButton;

    @InjectView(R.id.llContent)
    private LinearLayout llContent;

    @InjectView(R.id.pageIndicator)
    private CirclePageIndicator pageIndicator;

    @InjectView(R.id.rlViewPager)
    private RelativeLayout rlViewPager;

    private ArrayList<SelectedService> arrayScheduledServiceEntities = new ArrayList<>();

    public static MyScheduleFragment newInstance() {
        return new MyScheduleFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setListeners();
                setUI();
                if (preferenceHelper.getScheduledServices() != null && preferenceHelper.getScheduledServices().size() > 0) {
                    initViewPager();
                }
            }
        }, 200);
    }

    private void setListeners() {
        btnUberPickUp.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    private void setUI() {
        if (preferenceHelper.getScheduledServices() != null && preferenceHelper.getScheduledServices().size() > 0) {
            llBottomButton.setVisibility(View.VISIBLE);
            llContent.setVisibility(View.VISIBLE);
        } else {
            llBottomButton.setVisibility(View.GONE);
            llContent.setVisibility(View.GONE);
        }
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setTitleBarBackground(getResources().getColor(R.color.home_list_item_blue));
        titleBar.setSubHeading("My Schedule");
        titleBar.showLeftButton(R.drawable.btn_back_arrow_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMainActivity().checkRightMenu())
                    getMainActivity().popFragment();
            }
        });
        titleBar.showRightButton(R.drawable.btn_icon_hamburger_normal, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().toggleRightMenu();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_schedule, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUberPickUp:
                getMainActivity().openUberApp(String.valueOf(locationTracker.getLatitude()) + "," + String.valueOf(locationTracker.getLongitude()));
                break;
            case R.id.btnCancel:
                if (preferenceHelper.getScheduledServices() != null) {
                    final int position = viewPagerServices.getCurrentItem();
                    SelectedService entity = arrayScheduledServiceEntities.get(position);
                    DialogFactory.createMessageDialog2(getMainActivity(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            arrayScheduledServiceEntities.remove(position);
                            preferenceHelper.putScheduledServices(arrayScheduledServiceEntities);
                            setUI();
                            initViewPager();
                        }
                    }, "Are you sure, you want to cancel your appointment of " + entity.getServiceName() + "?").show();
                }
                break;
        }
    }

    private void initViewPager() {
        if (preferenceHelper.getScheduledServices() != null && preferenceHelper.getScheduledServices().size() > 0) {
            rlViewPager.setVisibility(View.VISIBLE);
            arrayScheduledServiceEntities.clear();
            arrayScheduledServiceEntities.addAll(preferenceHelper.getScheduledServices());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    final SchedulePagerAdapter pagerAdapter = new SchedulePagerAdapter(getChildFragmentManager());
                    viewPagerServices.setAdapter(pagerAdapter);
                    viewPagerServices.setCurrentItem(0);
                    updateSelectedService(0);
                    pageIndicator.setViewPager(viewPagerServices);
                    // We set this on the indicator, NOT the pager
                    pageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageSelected(int position) {
                            updateSelectedService(position);
                            pagerAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                        }
                    });
                    viewPagerServices.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageSelected(int position) {
                            updateSelectedService(position);
                            pagerAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                }
            }, 100);
        } else {
            rlViewPager.setVisibility(View.GONE);
        }
    }

    private class SchedulePagerAdapter extends FragmentPagerAdapter {

        public SchedulePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public Fragment getItem(int pos) {
            String entity = new GsonBuilder().create().toJson(arrayScheduledServiceEntities.get(pos));
            ScheduleViewPagerFragment pagerFragment = new ScheduleViewPagerFragment();
            Bundle extras = new Bundle();
            extras.putString("scheduledServiceEntity", entity);
            pagerFragment.setArguments(extras);
            return pagerFragment;
        }

        @Override
        public int getCount() {
            if (arrayScheduledServiceEntities.size() < 8) {
                return arrayScheduledServiceEntities.size();
            } else {
                return 8;
            }
        }
    }

    private void updateSelectedService(int position) {
        SelectedService entity = arrayScheduledServiceEntities.get(position);
        // Updating Other Values
        txtTime.setText(entity.getServiceDate() + " @ " + entity.getServiceTime());
        if (preferenceHelper.getSelectedServices() != null) {
            txtLocation.setText(preferenceHelper.getSelectedServices().getTitle().getRendered());
        } else {
            txtLocation.setText("No Location selected");
        }
    }
}